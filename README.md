# ORACLES SSFR Data Processing Code

---

## Folder Structure


```text
p3
   |_____YYYYMMDD
   |             |_____YYYYMMDD.cfg
   |             |_____*.OSA2
   |             |_____*.plt2
   |             |_____*.CG4
   |_____wvl-cal
   |
   |_____ang-cal
   |            |_____ssfr6
   |                       |_____LC1
   |                       |        |_____20160427 (notes: pre cal, zenith light collector plugged into nadir port)
   |                       |        |_____20170110 (notes: post cal)
   |                       |_____LC2
   |                                |_____20160428 (notes: pre cal)
   |                                |_____20170104 (notes: post cal)
   |_____rad-cal

er2
   |_____YYYYMMDD
   |             |_____YYYYMMDD.cfg
   |             |_____*.OSA2
   |_____wvl-cal
   |
   |_____ang-cal
   |            |_____ssfr5
   |                       |_____LC3
   |                       |        |_____20160323
   |                       |_____LC5
   |                                |_____20160322
   |                       |_____LC6
   |                                |_____20160322
   |_____rad-cal

pro
   |_____wvl-cal
   |
   |_____ang-cal
   |            |_____oracles_make_cos_LC1.pro
   |            |_____oracles_make_cos_LC2.pro
   |            |_____oracles_make_cos_LC3.pro
   |            |_____oracles_make_cos_LC5.pro
   |            |_____oracles_make_cos_LC6.pro
   |            |
   |            |_____oracles_read_cos_LC1.pro
   |            |_____oracles_read_cos_LC2.pro
   |            |_____oracles_read_cos_LC3.pro
   |            |_____oracles_read_cos_LC5.pro
   |            |_____oracles_read_cos_LC6.pro
   |            |
   |            |_____oracles_fit_cos.pro
   |            |_____oracles_read_osa2.pro
   |            |_____oracles_read_sks.pro
   |_____rad-cal

```


---

## Procedures

- ### Wavelength Calibration

  __Code__: `01_wvl-cal.pro`

  __Method__: obtain wavelength offset using HeNe laser, Hg 6035 lamp, and NIR laser.

  __Notes__: For SSFR data during ORACLES 2016, no wavelength calibration is needed.

- ### Angular Calibration

  __Code__: `02_ang-cal.pro`

    ```ascii
    IDL Version 8.5.1, Mac OS X (darwin x86_64 m64).
    (c) 2015, Exelis Visual Information Solutions, Inc., a subsidiary of Harris Corporation.
    Installation number: XXX-XXXX.
    Licensed for use by: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

    IDL> .r oracles_make_cos_LC1.pro
    IDL> make_cos
    ```

  __Method__: obtain wavelength-dependent cosine response function from calibration
  data collected in the lab.

  __Notes__:

- ### Radiometric Calibration

  __Code__: `03_rad-cal.pro`

  __Method__:

  __Notes__: The __Primary__ and __Transfer__ calibrations are done in the lab. __Primary__
  calibration uses the standard bulb, which comes with counts to flux (`W/(m^2 nm)`)
  relationship from manufacturer. __Transfer__ calibration uses the counts-to-flux relationship
  from the standard bulb to to derive the counts-to-flux relationship for the small lamp inside
  the field calibrator. The __Secondary__ calibrations are done in the field with the field
  calibrator.


---

### - Angular Calibration

  - Get cosine response for different wavelengths and for different angles.

  - __How to run the code?__

    For light collector, e.g. `LC1`, use the code `oracles_make_cos_LC1.pro`
    under directory `pro/ang-cal`.



### - Radiometric Calibration

  - Primary: done with standard bulb, get primary response;

  - Transfer: done with calibrator right after primary, get response for the calibrator;

  - Secondary: done with calibrator during field experiment.

---
## Data Processing

