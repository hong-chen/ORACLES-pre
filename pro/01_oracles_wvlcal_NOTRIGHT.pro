@read_sks10.pro
@legend_1.pro

pro process_abs_lines

path='/Users/sabrinacochrane/data/oracles/cal/'
files=file_search(path+'krypton_line/20160426*',count=numspectra)


spec_data=fltarr(257,numspectra)
abs='krypton'

  for i=0,numspectra-1 do begin
   read_SKS,files[i],count, eos, shutter, inttime, spect, spect_temps, spect_UTC, spect_DATE, spect_ind, specst, speced,darks, dark_temps, dark_UTC, dark_DATE, dark_ind, darkst, darked
   temp=mean(spect[0,*,3],dimension=1)-mean(darks[0,*,3],dimension=1)
   spec_data[*,i]=temp
endfor
 

   ; (2) spectrometer / cal settings / DO NOT MODIFY!*****
  nc = 256 ; # channels ; for channel definitions, use 0 to 255 (not 1 to 256)
  c_si1=[301.946,3.31877,0.00037585,-1.76779E-6,0]             ; module # 062762
  c_si2=[302.818,3.31912,0.000343831,-1.81135e-6,0]            ; module # 071332
  c_in1=[2202.33,-4.35275,-0.00269498,3.84968e-6,-2.33845e-8]  ; module # 070088
  c_in2=[2210.29,-4.5998,0.00102444,-1.60349E-5,1.29122E-8]    ; module # 083029 ("new one, fan up, NADIR, bottom shutter row")
  ll=findgen(nc)
  position='nadir'
  ; 1=zenith; 2=nadir
  if position eq 'zenith' then begin
    wl_si1=c_si1[0]+c_si1[1]*ll+c_si1[2]*ll^2+c_si1[3]*ll^3+c_si1[4]*ll^4
    wl_in1=c_in1[0]+c_in1[1]*ll+c_in1[2]*ll^2+c_in1[3]*ll^3+c_in1[4]*ll^4 & wl_in1 = reverse(wl_in1)
  endif
  if position eq 'nadir' then begin
    wl_si2=c_si2[0]+c_si2[1]*ll+c_si2[2]*ll^2+c_si2[3]*ll^3+c_si2[4]*ll^4
    wl_in2=c_in2[0]+c_in2[1]*ll+c_in2[2]*ll^2+c_in2[3]*ll^3+c_in2[4]*ll^4 & wl_in2 = reverse(wl_in2)
 endif

 wavelength= wl_in2

 plot, wavelength, spec_data[1:*,0], title='Absorption Lines Original Spectrum', charsize=2
 if abs eq 'mercury' then begin
oplot, [1529.597, 1529.597], [-200,800], linestyle=2  
oplot, [1013.997, 1013.997], [-200,800], linestyle=2   
oplot, [1128.741, 1128.741], [-200,800], linestyle=2   
oplot, [1357.021, 1357.021], [-200,800], linestyle=2
endif
 if abs eq 'krypton' then begin
   oplot,[1363.422, 1363.422], [-200,800], linestyle=2  
   oplot, [1816.732, 1816.732], [-200,800], linestyle=2
endif



window, 1
plot, wavelength, spec_data[1:*,1], title='Absorption Lines Original Spectrum', charsize=2

num =wavelength[255]-wavelength[0];CHECK THIS
;new_wl=indgen(13177.2)*0.1 +wavelength[0]
new_wl=findgen(num*10+1, increment=0.1)+wavelength[0]
fit=spline(wavelength, spec_data[1:*,0], new_wl)


if abs eq 'mercury' then begin

window, 2
plot, new_wl, fit, title='Mercury Lines Cubic Spline (high res)', charsize=2, xrange=[700,2250]
oplot, [1529.597, 1529.597], [-200,800], linestyle=2  
oplot, [1013.997, 1013.997], [-200,800], linestyle=2   
oplot, [1128.741, 1128.741], [-200,800], linestyle=2

;zoom in and find fwhm for 3 lines

;1013
window, 3
plot, new_wl, fit, title='Hg Lines Cubic Spline (high res) 1013 line', charsize=2, xrange=[950,1050]
oplot, [1013.997, 1013.997], [-200,800], linestyle=2
oplot, wavelength, spec_data[1:*,0], color=60, linestyle=2
cursor, x0,y0
oplot, [x0,x0], [-200,600], linestyle=1, color=30
max_1013=y0
oplot, [950,1050], [max_1013/2, max_1013/2], linestyle=2, color=30
wait, 0.2
cursor, x1, y1
oplot, [x1,x1], [-200,600], linestyle=1, color=30
wait, 0.2

cursor, x2, y2
oplot, [x2,x2], [-200,600], linestyle=1, color=30
fwhm_1013=x2-x1

xyouts, [945],[400],'line position:'+strcompress(string(x0)), charsize=2
xyouts, [945],[350],'FWHM:'+strcompress(string(fwhm_1013)), charsize=2




;1128
window, 4
plot, new_wl, fit, title='Hg Lines Cubic Spline (high res) 1128 line', charsize=2, xrange=[1100,1150]
oplot, [1128.741, 1128.741], [-200,800], linestyle=2
oplot, wavelength, spec_data[1:*,0], color=60, linestyle=2
cursor, x3,y3
oplot, [x3,x3], [-200,600], linestyle=1, color=30
max_1128=y3
oplot, [1100,1150], [max_1128/2, max_1128/2], linestyle=2
wait, 0.2
cursor, x4, y4
oplot, [x4,x4], [-200,600], linestyle=1, color=30
wait, 0.2
cursor, x5, y5
oplot, [x5,x5], [-200,600], linestyle=1, color=30
fwhm_1128=x5-x4

xyouts, [1101],[250],'line position:'+strcompress(string(x3)), charsize=2
xyouts, [1101],[200],'FWHM:'+strcompress(string(fwhm_1128)), charsize=2


;1529
window, 5
plot, new_wl, fit, title='Hg Lines Cubic Spline (high res) 1529 line', charsize=2, xrange=[1500,1550]
oplot, [1529.597, 1529.597], [-200,800], linestyle=2
oplot, wavelength, spec_data[1:*,0], color=60, linestyle=2
cursor, x6,y6
oplot, [x6,x6], [-200,800], linestyle=1, color=30
max_1529=y6
oplot, [1500,1550], [max_1529/2, max_1529/2], linestyle=2
wait, 0.2
cursor, x7, y7
oplot, [x7,x7], [-200,800], linestyle=1, color=30
wait, 0.2
cursor, x8, y8
oplot, [x8,x8], [-200,800], linestyle=1, color=30
fwhm_1529=x8-x7

xyouts, [1501],[700],'line position:'+strcompress(string(x6)), charsize=2
xyouts, [1501],[575],'FWHM:'+strcompress(string(fwhm_1529)), charsize=2
endif

if abs eq 'krypton' then begin
   window, 6
plot, new_wl, fit, title='krypton Line Cubic Spline (high res) 1363.422 line', charsize=2, xrange=[1340,1375]
oplot, [1363.422, 1363.422], [-200,800], linestyle=2
oplot, wavelength, spec_data[1:*,0], color=60, linestyle=2
cursor, x9,y9
oplot, [x9,x9], [-200,800], linestyle=1, color=30
max_1363=y9
oplot, [1340,1380], [max_1363/2, max_1363/2], linestyle=2
wait, 0.2
cursor, x10, y10
oplot, [x10,x10], [-200,800], linestyle=1, color=30
wait, 0.2
cursor, x11, y11
oplot, [x11,x11], [-200,800], linestyle=1, color=30
fwhm_1363=x11-x10

xyouts, [1342],[90],'line position:'+strcompress(string(x9)), charsize=2
xyouts, [1342],[80],'FWHM:'+strcompress(string(fwhm_1363)), charsize=2

window, 7
plot, new_wl, fit, title='krypton Lines Cubic Spline (high res) 1816.732 line', charsize=2, xrange=[1790,1830]
oplot, [1816.732, 1816.732], [-200,800], linestyle=2
oplot, wavelength, spec_data[1:*,0], color=60, linestyle=2
cursor, x12,y12
oplot, [x12,x12], [-200,800], linestyle=1, color=30
max_1816=y12
oplot, [1790,1830], [max_1816/2, max_1816/2], linestyle=2
wait, 0.2
cursor, x13, y13
oplot, [x13,x13], [-200,800], linestyle=1, color=30
wait, 0.2
cursor, x14, y14
oplot, [x14,x14], [-200,800], linestyle=1, color=30
fwhm_1816=x14-x13

xyouts, [1792],[175],'line position:'+strcompress(string(x12)), charsize=2
xyouts, [1792],[150],'FWHM:'+strcompress(string(fwhm_1816)), charsize=2
   
endif 



stop
end
