; Program makes interpolated cosine response functions,
; based on processed raw data.

@legend_1.pro
@fit_cos.pro
pro make_cos


  ; +----------------------------------------------------------
  ; User setttings
  path='/Users/sabrinacochrane/data/oracles/' ; experiment directory
  resp='er2/ang/cosine/ssfr5/LC6_angular.sav'    ; IDL save file to be processed
  off = 0.08                          ; offset (below which response won't be valid)
  ch1 = 100                           ; Si channel to display & process
  ch2 = 400                           ; InGaAs channel to display & process
  png=path+'er2/ang/cosine/cos_resp.png'
  out='/Users/sabrinacochrane/data/oracles/er2/ang/cosine/ssfr5/20160801_lc6.out'; IDK what date this is supposed to be
  ; +----------------------------------------------------------

  ; settings
  file=path+resp
  device,decomposed=0
  loadct,27

  ; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ; get data
  ; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  restore,file
  ang=angles                      ; transfer data from file into variables
  nl =n_elements(ang)
  res = fltarr(nl,514) ; angular response
  wl  = fltarr(514)    ; wl information --> Need to get from Bruce for our system
  res[*,0:256]=transpose(cosinedata); check zero element,
  ;res[*,257:513]=transpose(cosinedatanir); check here too. get rid of zeroth element because just identifier
  mu       = cos(!pi/180.*ang)

; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
; plots
; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
window,0,title='raw data',retain=2
plot ,mu,res[*,ch1]/res[0,ch1],psym=2,xtit='cos(ang)',ytit='response',chars=2,tit=resp
oplot,mu,res[*,ch2]/res[0,ch2],psym=2,color=120
oplot,[0,1],[0,1],linesty=1,thick=2
legend_1,[string(ch1),string(ch2)],textcolor=[255,120]

; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
; fit the data
; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
lo = 0.7 ; below this, use linear fit
hi = 0.9 ; above this, use polynamial

res0=res[0,ch1]
fit_cos,mu,res[*,ch1]/res0,lo,hi,mu_grd,sp_grd;,max_error=1.5;,/verbose
flt=where(mu_grd lt off)
sp_grd[flt]=0
oplot,mu_grd,sp_grd

p=tvrd(true=1)
write_png,png,p

diffuse=0.5/(total(sp_grd)/float(n_elements(mu_grd)))
print,'Diffuse correction:',diffuse

save,file=out,mu_grd,sp_grd,diffuse

stop
end
