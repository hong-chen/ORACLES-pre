;+
; NAME:
;   cal
;
; PURPOSE:
;   Read in the data for calibrations, make response functions from primary calibration and secondary
;
; CATEGORY:
;   ORACLES / Calibration
;
; CALLING SEQUENCE:
;   cal
;
; OUTPUT:
;   - Plots of the calibration
;
; KEYWORDS:
;
; DEPENDENCIES:
;   cfg.pro       ; read config file
;   legend.pro    ; to make legend on graph
;
; NEEDED FILES:
;   - config file for calib
;   - all ".OSA2" files for the calibs under the proper directories
;
; EXAMPLE:
;   cal
;
; MODIFICATION HISTORY:
; Written:  Shi Song, July 8th, 2013, LASP/CU Boulder
;           - ported from podex_cal.pro
;           - this code is for processing the SSFR-Ames calibrations,
;             not SSFR-CU calibrations
; Modified:
;
; TBD:
; Read in multiple files if necessary.
; version _int - allows adjustment for different Si integration times.
;---------------------------------------------------------------------------

@sub_cfg.pro
@legend_1.pro

pro cal

  ; +----------------------------------------------------------
  ;  Path settings
  cfg_file='/Users/sabrinacochrane/data/oracles/p3/cal/ssfr6/ssfr6_cal.cfg'
  l       ='/' ; separator
  ; +----------------------------------------------------------

  ; +----------------------------------------------------------
  ;  Code map
  ;    Module                                 Output
  ;    (1) Read configuration file
  ;    (2) Process primary calibration    --> primary response function
  ;    (3) Process transfer calibration   --> field calibrator irradiance spectra
  ;    (4) Process secondary calibration  --> secondary response function
  ; +----------------------------------------------------------
  ;  Graphics and other useful settings
  plt=1 ; 1: plot, 0: no plots
  loadct,27,/silent
  device,decomposed=0
  ;!p.color=0
  ;!p.background=255
  !P.multi=0
  months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
  ; +----------------------------------------------------------

  ; +----------------------------------------------------------
  ;  (1) Read configuration file
  if file_test(cfg_file) ne 1 then message,'Did not find configuration file.'

  np                    = fix(cfg(cfg_file,'np'  ))    ; number of channels for each spectrum
  ;innp_used             = fix(cfg(cfg_file,'innp_used'))    ; number of channels used in the IR

  platform              = cfg(cfg_file,'platform')
  path                  = cfg(cfg_file,'cal_directory')
  rpath                 = path+cfg(cfg_file,'resp_subdirectory')+l
  data                  = cfg(cfg_file,'data')    ; data file extension

  primary_lamp          = strsplit(cfg(cfg_file,'primary_lamp') ,', ',escape='#',/extract)
  primary_lamp_serial   = primary_lamp[0]
  primary_lamp_file     = primary_lamp[1]
  primary_cal_date      = strsplit(cfg(cfg_file,'primary_cal_date') ,', ',escape='#',/extract) ; array of primary lamp

  secondary_cal_date    = strsplit(cfg(cfg_file,'secondary_cal_date') ,', ',escape='#',/extract) ; to make an array of secondary cals.
  secondary_lamp        = cfg(cfg_file,'secondary_lamp') ; serial # secondary lamp

  inttime_nadir         = strsplit(cfg(cfg_file,'inttime_nadir') ,', ',escape='#',/extract)
  inttime_zenith        = strsplit(cfg(cfg_file,'inttime_zenith') ,', ',escape='#',/extract)

  line_color            = strsplit(cfg(cfg_file,'line_color'),', ',escape='#',/extract)
  line_style            = strsplit(cfg(cfg_file,'line_style'),', ',escape='#',/extract)
  line_thick            = strsplit(cfg(cfg_file,'line_thick'),', ',escape='#',/extract)
  zsifirst              = float(cfg(cfg_file,'zsifirst'))
  zjoin                 = float(cfg(cfg_file,'zjoin'))
  zirlast               = float(cfg(cfg_file,'zirlast'))
  nsifirst              = float(cfg(cfg_file,'nsifirst'))
  njoin                 = float(cfg(cfg_file,'njoin'))
  nirlast               = float(cfg(cfg_file,'nirlast'))

  dozenith              = cfg(cfg_file,'dozenith')
  donadir               = cfg(cfg_file,'donadir')
  write_primary         = cfg(cfg_file,'write_primary')

  num_secondary = n_elements(secondary_cal_date)
  if num_secondary gt 1 then print, 'Multiple secondary calibrations, plotting all for comparison.'

  ; read file location for primary
  primary_nadir_si           = strsplit(cfg(cfg_file,'primary_nadir_si')   ,', ',escape='#',/EXTRACT)
  label=primary_nadir_si[0]  & inttime_nadir_si=primary_nadir_si[1]
  primary_nadir_si           = path+primary_cal_date+l+primary_lamp_serial+l+'nadir'+l+label+l
  primary_nadir_ir           = strsplit(cfg(cfg_file,'primary_nadir_ir')   ,', ',escape='#',/EXTRACT)
  label=primary_nadir_ir[0]  & inttime_nadir_ir=primary_nadir_ir[1]
  primary_nadir_ir           = path+primary_cal_date+l+primary_lamp_serial+l+'nadir'+l+label+l

  primary_zenith_si          = strsplit(cfg(cfg_file,'primary_zenith_si')   ,', ',escape='#',/EXTRACT)
  label=primary_zenith_si[0] & inttime_zenith_si=primary_zenith_si[1]
  primary_zenith_si          = path+primary_cal_date+l+primary_lamp_serial+l+'zenith'+l+label+l
  primary_zenith_ir          = strsplit(cfg(cfg_file,'primary_zenith_ir')   ,', ',escape='#',/EXTRACT)
  label=primary_zenith_ir[0] & inttime_zenith_ir=primary_zenith_ir[1]
  primary_zenith_ir          = path+primary_cal_date+l+primary_lamp_serial+l+'zenith'+l+label+l

  ; read file location for transfer
  transfer_nadir_si           = strsplit(cfg(cfg_file,'transfer_nadir_si')   ,', ',escape='#',/EXTRACT)
  label=transfer_nadir_si[0]
  transfer_nadir_si           = path+primary_cal_date+l+secondary_lamp+l+'nadir'+l+label+l
  transfer_nadir_ir           = strsplit(cfg(cfg_file,'transfer_nadir_ir')   ,', ',escape='#',/EXTRACT)
  label=transfer_nadir_ir[0]
  transfer_nadir_ir           = path+primary_cal_date+l+secondary_lamp+l+'nadir'+l+label+l

  transfer_zenith_si          = strsplit(cfg(cfg_file,'transfer_zenith_si')   ,', ',escape='#',/EXTRACT)
  label=transfer_zenith_si[0]
  transfer_zenith_si          = path+primary_cal_date+l+secondary_lamp+l+'zenith'+l+label+l
  transfer_zenith_ir          = strsplit(cfg(cfg_file,'transfer_zenith_ir')   ,', ',escape='#',/EXTRACT)
  label=transfer_zenith_ir[0]
  transfer_zenith_ir          = path+primary_cal_date+l+secondary_lamp+l+'zenith'+l+label+l

  ; read file location for secondary
  secondary_nadir_si           = strsplit(cfg(cfg_file,'secondary_nadir_si')   ,', ',escape='#',/EXTRACT)
  label=secondary_nadir_si[0]
  secondary_nadir_si           = path+secondary_cal_date+l+secondary_lamp+l+'nadir'+l+label+l
  secondary_nadir_ir           = strsplit(cfg(cfg_file,'secondary_nadir_ir')   ,', ',escape='#',/EXTRACT)
  label=secondary_nadir_ir[0]
  secondary_nadir_ir           = path+secondary_cal_date+l+secondary_lamp+l+'nadir'+l+label+l

  secondary_zenith_si          = strsplit(cfg(cfg_file,'secondary_zenith_si')   ,', ',escape='#',/EXTRACT)
  label=secondary_zenith_si[0]
  secondary_zenith_si          = path+secondary_cal_date+l+secondary_lamp+l+'zenith'+l+label+l
  secondary_zenith_ir          = strsplit(cfg(cfg_file,'secondary_zenith_ir')   ,', ',escape='#',/EXTRACT)
  label=secondary_zenith_ir[0]
  secondary_zenith_ir          = path+secondary_cal_date+l+secondary_lamp+l+'zenith'+l+label+l

  ; wavelengths
  wlvisz=fltarr(np) & wlnirz=fltarr(np)
  wlvisn=fltarr(np) & wlnirn=fltarr(np)

  ; make wavelengths / zenith
  zlambdasi=cfg(cfg_file,'zlambdaSI') ; wavelength coefficients SI
  zlambdasi=strsplit(zlambdasi,' ,',count=znlsi,escape='#',/EXTRACT) ; make string into substrings
  zlambdasi=float(zlambdasi)
  zlambdair=cfg(cfg_file,'zlambdaIR') ; wavelength coefficients IR
  zlambdair=strsplit(zlambdair,' ,',count=znlir,escape='#',/EXTRACT) ; make string into substrings
  zlambdair=float(zlambdair)
  wlvisz=fltarr(np) & wlnirz=fltarr(np)
  for i=0,np-1 do begin
    for j=0,znlsi-1 do wlvisz[i]     =wlvisz[i]     +zlambdasi[j]*float(i)^j
    for j=0,znlir-1 do wlnirz[np-1-i]=wlnirz[np-1-i]+zlambdair[j]*float(i)^j
  endfor

  ; make wavelengths / nadir
  nlambdasi=cfg(cfg_file,'nlambdaSI') ; wavelength coefficients SI
  nlambdasi=strsplit(nlambdasi,' ,',count=nnlsi,escape='#',/EXTRACT) ; make string into substrings
  nlambdasi=float(nlambdasi)
  nlambdair=cfg(cfg_file,'nlambdaIR') ; wavelength coefficients IR
  nlambdair=strsplit(nlambdair,' ,',count=nnlir,escape='#',/EXTRACT) ; make string into substrings
  nlambdair=float(nlambdair)
  wlvisn=fltarr(np) & wlnirn=fltarr(np)
  for i=0,np-1 do begin
    for j=0,nnlsi-1 do wlvisn[i]     =wlvisn[i]     +nlambdasi[j]*float(i)^j
    for j=0,nnlir-1 do wlnirn[np-1-i]=wlnirn[np-1-i]+nlambdair[j]*float(i)^j
  endfor

  ; Read temperature coefficients
  temp   =path+cfg(cfg_file,'thermistor')
  npt  = file_lines(temp)
  tdat = fltarr(2, npt)   & revt = fltarr(2, npt)
  openr,ut,temp, /get_lun & readf,ut,tdat & free_lun,ut
  for i = 0, npt - 1 do begin
    revt[0, i] = tdat[0, npt -i - 1] & revt[1, i] = tdat[1, npt -i - 1]
  endfor
  rt    = findgen(20000)/10000. + 0. ; interpolate thermistor data
  revts = spline(revt(1, *), revt(0, *), rt) ; at fine resolution
  ; +----------------------------------------------------------

  ; +----------------------------------------------------------
  ;  (2) Process primary calibration
  bn    = 200 ; max # of spectra in a file
  spec  = {btime: lonarr(2), bcdtimstp:  bytarr(12),intime1: long(0),$
           intime2: long(0), intime3: long(0), intime4: long(0),accum: long(0), shsw:long(0),$
           zsit: ulong(0),nsit: ulong(0),zirt: ulong(0),nirt: ulong(0), zirx: ulong(0), nirx: ulong(0),xt: ulong(0),it: ulong(0),$
           zspecsi: intarr(np), zspecir: intarr(np), nspecsi: intarr(np), nspecir: intarr(np)}
  spc   = fltarr(2,2,2,np)     ; spectra (nadir/zenith,Silicon/InGaAs,dark/cal,# channels)
  tem   = fltarr(2,2,2)        ; temperatures of relevant spectrometers
  rl    = ['Silicon','InGaAs'] ; spectrometer label
  for i=0,1 do begin           ; 0:nadir, 1:zenith
    for j=0,1 do begin           ; 0:Silicon, 1:InGaAs
      if i eq 0 then begin ; nadir
        dir='nadir'
        chk=donadir
        if j eq 0 then begin ; Silicon
          rad=primary_nadir_si
          int=fix(inttime_nadir_si)
        endif else begin     ; InGaAs
          rad=primary_nadir_ir
          int=fix(inttime_nadir_ir)
        endelse
      endif else begin     ; zenith
        dir='zenith'
        chk=dozenith
        if j eq 0 then begin ; Silicon
          rad=primary_zenith_si
          int=fix(inttime_zenith_si)
        endif else begin     ; InGaAs
          rad=primary_zenith_ir
          int=fix(inttime_zenith_ir)
        endelse
      endelse

      if(strcmp(chk,'yes')) then begin ; check if this direction (zenith/nadir) should be processed
        for k=0,1 do begin                          ; 0:dark, 1:cal
          if k eq 0 then file=rad+'dark'+l else file=rad+'cal'+l
          spc_files = file_search(file+data, count = numfiles, /FOLD_CASE)
          if (numfiles ne 1) then message,'There should be exactly 1 file for the dark.'
          openr,lunn,spc_files[0],/get_lun
          ctn=0L
          spt  = fltarr(np,bn) ; spectra time series
          t    = fltarr(bn)    ; temperature time series
          while not eof(lunn) do begin
            readu, lunn, spec
            if i eq 0 then begin   ; nadir
              if j eq 0 then begin ; Silicon
                dt =spec.intime3
                sp0=spec.nspecsi
                ch1 = ((spec.nsit/2048.)*5.) - 5.   ; Nadir Si spect temp.
                nsit = long(ch1 * 10000.)     ;convert to integer for use as index
                if (nsit le 19999) then t0 = revts[nsit]       ;nadir si temp
              endif else begin     ; InGaAs
                dt =spec.intime4
                sp0=spec.nspecir
                ch7=((spec.nirx/2048.)*5.) - 5. & rtem = abs((2000. * ch7)/(1. - (0.2 * ch7))) &  dem = 1.0295e-3+(2.391e-4 * alog(rtem)) + (1.568e-7 * (alog(rtem))^3)
                t0 = float(1./dem - 273.) ; temperature
              endelse
            endif else begin       ; zenith
              if j eq 0 then begin ; Silicon
                dt =spec.intime1
                sp0=spec.zspecsi
                ch0 = ((spec.zsit/2048.)*5.) - 5.   ; Zenith SI spect temp.
                zsit = long(ch0 * 10000.)     ;convert to integer for use as index
                if (zsit le 19999) then t0 = revts[zsit]       ;load zenith temp into array
              endif else begin     ; InGaAs
                dt =spec.intime2
                sp0=spec.zspecir
                ch7=((spec.zirx/2048.)*5.) - 5. & rtem = abs((2000. * ch7)/(1. - (0.2 * ch7))) & dem = 1.0295e-3+(2.391e-4 * alog(rtem)) + (1.568e-7 * (alog(rtem))^3)
                t0 = float(1./dem - 273.) ; temperature
             endelse
           endelse
           if dt ne int then print,'Warning: integration times do not match '+dir+rl[j]
           spt[*,ctn]=sp0
           t  [  ctn]=t0
           ctn = ctn + 1L
         endwhile
         free_lun,lunn
         for ch=0,np-1 do begin
           spc[i,j,k,ch]=mean(spt[ch,0:ctn-1])
         endfor
         tem[i,j,k]=mean(t[0:ctn-1])
       endfor ; k loop (dark/cal)
       ; check date
       atime  = systime(0, spec.btime[0], /utc) ; convert date
       result = strpos(atime(0), ':', 0) ; find first incidence of ':'
       day1 =  strmid(atime(0), result - 5, 1) &  day2 =  strmid(atime(0), result - 4, 1)
       if(day1 eq ' ') then day = '0' + day2 else  day = day1 + day2
       mon =  strmid(atime(0), result - 9, 3) & mon = strtrim(string(where(months eq mon) + 1),1)
       if(mon lt 10) then mon = '0' + string(mon[0]) else mon = string(mon[0])
       year = fix(strmid(atime(0), result + 7, 4)) ;get year
       date = strtrim(string(year,mon,day),1)
       if not strcmp(primary_cal_date,date,8) then print,'Warning: Primary cal date incorrect:'+primary_cal_date+' '+date
      endif ; do direction (nadir/zenith)
    endfor  ; j loop (Silicon/InGaAs)
  endfor  ; i loop (nadir/zenith)

  ; read in lamp data
  nl=file_lines(path+primary_lamp_file)
  openr,lunl,path+primary_lamp_file,/get_lun
  pl=fltarr(nl) & pi=fltarr(nl)
  for i=0,nl-1 do begin
    readf,lunl,a,b
    pl[i]=a & pi[i]=b*1E4
  endfor
  free_lun,lunl
  lampzensi = spline(pl,pi,wlvisz) ; spline lamp response to SSFR wavelengths
  lampzenir = spline(pl,pi,wlnirz)
  lampnadsi = spline(pl,pi,wlvisn) ; spline lamp response to SSFR wavelengths
  lampnadir = spline(pl,pi,wlnirn)

  ; calculate primary response functions
  ; 0 - zen si, 1 - zen ir, 2 - nad si, 3 - nad ir
  resp1=fltarr(np,4)
  temp1=fltarr(2,4)  ; 2: cal&dark

  resp1[*,0] = (spc[1,0,1,*]-spc[1,0,0,*])/float(inttime_zenith_si)/(lampzensi)
  resp1[*,1] = reverse(reform(spc[1,1,1,*]-spc[1,1,0,*]))/float(inttime_zenith_ir)/(lampzenir)
  resp1[*,2] = (spc[0,0,1,*]-spc[0,0,0,*])/float(inttime_nadir_si)/(lampnadsi)
  resp1[*,3] = reverse(reform(spc[0,1,1,*]-spc[0,1,0,*]))/float(inttime_nadir_ir)/(lampnadir)
  temp1[*,0] = tem[1,0,*]
  temp1[*,1] = tem[1,1,*]
  temp1[*,2] = tem[0,0,*]
  temp1[*,3] = tem[0,1,*]

  ; save response functions
  if(strcmp(write_primary,'yes')) then begin
    if (strcmp(donadir,'yes')) then begin
      resp1_si_file = rpath + primary_cal_date + '_'+primary_lamp_serial+'_resp1_'+strcompress(string(inttime_nadir_si),/REMOVE_ALL)+'_nadsi.dat'
      resp1_ir_file = rpath + primary_cal_date + '_'+primary_lamp_serial+'_resp1_'+strcompress(string(inttime_nadir_ir),/REMOVE_ALL)+'_nadir.dat'
      openw,us,resp1_si_file,/get_lun
      openw,ui,resp1_ir_file,/get_lun
      for i=0,np-1 do begin
        printf,us,wlvisn[i],resp1[i,2]
        printf,ui,wlnirn[i],resp1[i,3]
      endfor
      free_lun,us
      free_lun,ui
    endif
    if (strcmp(dozenith,'yes')) then begin
      resp1_si_file = rpath + primary_cal_date + '_'+primary_lamp_serial+'_resp1_'+strcompress(string(inttime_zenith_si),/REMOVE_ALL)+'_zensi.dat'
      resp1_ir_file = rpath + primary_cal_date + '_'+primary_lamp_serial+'_resp1_'+strcompress(string(inttime_zenith_ir),/REMOVE_ALL)+'_zenir.dat'
      openw,us,resp1_si_file,/get_lun
      openw,ui,resp1_ir_file,/get_lun
      for i=0,np-1 do begin
        printf,us,wlvisz[i],resp1[i,0]
        printf,ui,wlnirz[i],resp1[i,1]
      endfor
      free_lun,us
      free_lun,ui
    endif
    resp1_save = rpath + primary_cal_date + '_'+primary_lamp_serial+'_resp1_60.sav'
    save,file=resp1_save,resp1,temp1,inttime_zenith_si,inttime_zenith_ir,inttime_nadir_si,inttime_nadir_ir, wlvisz, wlvisn, wlnirz, wlnirn
  endif


  ; make plots
  if plt then begin
    !P.multi=[0,1,2]
    window,1,retain=2,xs=700,ys=800
    plot,wlvisz,resp1[*,0],xtit='Wavelength [nm]',ytit='DN/ms / W/m2/nm',tit='Primary Response Functions (Silicon)',chars=2,xr=[300,1200],/xs
    oplot,wlvisn,resp1[*,2],color=120
    legend_1,['zenith','nadir'],textcolor=[255,120]
    ;window,1,retain=2,xs=700,ys=500
    plot,wlnirz,resp1[*,1],xtit='Wavelength [nm]',ytit='DN/ms / W/m2/nm',tit='Primary Response Functions (InGaAs)',chars=2,xr=[900,2200],/xs,yr=[0,200]
    oplot,wlnirn,resp1[*,3],color=120
    legend_1,['zenith','nadir'],textcolor=[255,120]
    !P.multi=0
    p=tvrd(true=1)
    ;write_png,path+date+l+primary_lamp_serial+'_response1.png',p
  endif
  ; +----------------------------------------------------------
  ;stop
  ; +----------------------------------------------------------
  ;  (3) Process transfer calibration
  bn    = 200 ; max # of spectra in a file
  spec  = {btime: lonarr(2), bcdtimstp:  bytarr(12),intime1: long(0),$
           intime2: long(0), intime3: long(0), intime4: long(0),accum: long(0), shsw:long(0),$
           zsit: ulong(0),nsit: ulong(0),zirt: ulong(0),nirt: ulong(0), zirx: ulong(0), nirx: ulong(0),xt: ulong(0),it: ulong(0),$
           zspecsi: intarr(np), zspecir: intarr(np), nspecsi: intarr(np), nspecir: intarr(np)}
  spc   = fltarr(2,2,2,np)     ; spectra (nadir/zenith,Silicon/InGaAs,dark/cal,# channels)
  tem   = fltarr(2,2,2)        ; temperatures of relevant spectrometers
  rl    = ['Silicon','InGaAs'] ; spectrometer label
  for i=0,1 do begin           ; 0:nadir, 1:zenith
    for j=0,1 do begin           ; 0:Silicon, 1:InGaAs
      if i eq 0 then begin ; nadir
        dir='nadir'
        chk=donadir
        if j eq 0 then begin ; Silicon
          rad=transfer_nadir_si
          int=fix(inttime_nadir_si)
        endif else begin     ; InGaAs
          rad=transfer_nadir_ir
          int=fix(inttime_nadir_ir)
        endelse
      endif else begin     ; zenith
        dir='zenith'
        chk=dozenith
        if j eq 0 then begin ; Silicon
          rad=transfer_zenith_si
          int=fix(inttime_zenith_si)
        endif else begin     ; InGaAs
          rad=transfer_zenith_ir
          int=fix(inttime_zenith_ir)
        endelse
      endelse

      if(strcmp(chk,'yes')) then begin ; check if this direction (zenith/nadir) should be processed
        for k=0,1 do begin                          ; 0:dark, 1:cal
          if k eq 0 then file=rad+'dark'+l else file=rad+'cal'+l
          spc_files = file_search(file+data, count = numfiles, /FOLD_CASE)
          if (numfiles ne 1) then message,'There should be exactly 1 file for the dark.'
          openr,lunn,spc_files[0],/get_lun
          ctn=0L
          spt  = fltarr(np,bn) ; spectra time series
          t    = fltarr(bn)    ; temperature time series
          while not eof(lunn) do begin
            readu, lunn, spec
            if i eq 0 then begin   ; nadir
              if j eq 0 then begin ; Silicon
                dt =spec.intime3
                sp0=spec.nspecsi
                ch1 = ((spec.nsit/2048.)*5.) - 5.   ; Nadir Si spect temp.
                nsit = long(ch1 * 10000.)     ;convert to integer for use as index
                if (nsit le 19999) then t0 = revts[nsit]       ;nadir si temp
              endif else begin     ; InGaAs
                dt =spec.intime4
                sp0=spec.nspecir
                ch7=((spec.nirx/2048.)*5.) - 5. & rtem = abs((2000. * ch7)/(1. - (0.2 * ch7))) &  dem = 1.0295e-3+(2.391e-4 * alog(rtem)) + (1.568e-7 * (alog(rtem))^3)
                t0 = float(1./dem - 273.) ; temperature
              endelse
            endif else begin       ; zenith
              if j eq 0 then begin ; Silicon
                dt =spec.intime1
                sp0=spec.zspecsi
                ch0 = ((spec.zsit/2048.)*5.) - 5.   ; Zenith SI spect temp.
                zsit = long(ch0 * 10000.)     ;convert to integer for use as index
                if (zsit le 19999) then t0 = revts[zsit]       ;load zenith temp into array
              endif else begin     ; InGaAs
                dt =spec.intime2
                sp0=spec.zspecir
                ch7=((spec.zirx/2048.)*5.) - 5. & rtem = abs((2000. * ch7)/(1. - (0.2 * ch7))) & dem = 1.0295e-3+(2.391e-4 * alog(rtem)) + (1.568e-7 * (alog(rtem))^3)
                t0 = float(1./dem - 273.) ; temperature
              endelse
            endelse
            if dt ne int then message,'integration times do not match '+dir+rl[j]
            spt[*,ctn]=sp0
            t  [  ctn]=t0
            ctn = ctn + 1L
          endwhile
          free_lun,lunn
          for ch=0,np-1 do begin
            spc[i,j,k,ch]=mean(spt[ch,0:ctn-1])
          endfor
          tem[i,j,k]=mean(t[0:ctn-1])
        endfor ; k loop (dark/cal)
        ; check date
        atime  = systime(0, spec.btime[0], /utc) ; convert date
        result = strpos(atime(0), ':', 0) ; find first incidence of ':'
        day1 =  strmid(atime(0), result - 5, 1) &  day2 =  strmid(atime(0), result - 4, 1)
        if(day1 eq ' ') then day = '0' + day2 else  day = day1 + day2
        mon =  strmid(atime(0), result - 9, 3) & mon = strtrim(string(where(months eq mon) + 1),1)
        if(mon lt 10) then mon = '0' + string(mon[0]) else mon = string(mon[0])
        year = fix(strmid(atime(0), result + 7, 4)) ;get year
        date = strtrim(string(year,mon,day),1)
        if not strcmp(primary_cal_date,date,8) then print,'Warning: Transfer cal date incorrect:'+primary_cal_date+' '+date
      endif ; do direction (nadir/zenith)
    endfor  ; j loop (Silicon/InGaAs)
  endfor  ; i loop (nadir/zenith)

  ; calculate secondary lamp (calibrator) output spectrum
  ; 0 - zen si, 1 - zen ir, 2 - nad si, 3 - nad ir
  lamp2=fltarr(np,4)
  tempx=fltarr(2,4)  ; 2: cal&dark

  lamp2[*,0] = (spc[1,0,1,*]-spc[1,0,0,*])/float(inttime_zenith_si)/resp1[*,0]
  lamp2[*,1] = reverse(reform(spc[1,1,1,*]-spc[1,1,0,*]))/float(inttime_zenith_ir)/resp1[*,1]
  lamp2[*,2] = (spc[0,0,1,*]-spc[0,0,0,*])/float(inttime_nadir_si)/resp1[*,2]
  lamp2[*,3] = reverse(reform(spc[0,1,1,*]-spc[0,1,0,*]))/float(inttime_nadir_ir)/resp1[*,3]
  tempx[*,0] = tem[1,0,*]
  tempx[*,1] = tem[1,1,*]
  tempx[*,2] = tem[0,0,*]
  tempx[*,3] = tem[0,1,*]
  lamp_save = rpath + secondary_cal_date + '_'+secondary_lamp+'_resp2.sav'
  save,file=lamp_save,wlvisn,wlnirn,wlvisz,wlnirz,lamp2,tempx

  ; make plots
  if plt then begin
     !P.multi=[0,1,2]
     window,2,retain=2,xs=700,ys=700
     plot,wlvisz,lamp2[*,0],xtit='Wavelength [nm]',ytit='W m!U-2!N nm!U-1!N',tit=platform+' '+secondary_lamp+' irradiance (zenith)',chars=2,xr=[300,2200],/xs,yr=[0,0.35],thick=3
     oplot,wlnirz,lamp2[*,1],color=70,thick=3
     oplot,[zsifirst,zsifirst],[0,100],linesty=2
     oplot,[zjoin,zjoin],[0,100],linesty=2
     oplot,[zirlast,zirlast],[0,100],linesty=2
     oplot,wlvisz,lampzensi,linesty=2
     oplot,wlnirz,lampzenir,color=70,linesty=2
     legend_1,['Silicon','InGaAs'],textcolor=[255,70],chars=2
     legend_1,[strjoin(tempx[*,0])+' deg',strjoin(tempx[*,1])+' deg'],textcolor=[255,70],/right
     plot,wlvisn,lamp2[*,2],xtit='Wavelength [nm]',ytit='W/m2/nm',tit=platform+' '+secondary_lamp+' irradiance (nadir)',chars=2,xr=[300,2200],/xs,yr=[0,0.35],thick=3
     oplot,wlnirn,lamp2[*,3],color=70,thick=3
     oplot,[nsifirst,nsifirst],[0,100],linesty=2
     oplot,[njoin,njoin],[0,100],linesty=2
     oplot,[nirlast,nirlast],[0,100],linesty=2
     oplot,wlvisn,lampnadsi,linesty=2
     oplot,wlnirn,lampnadir,color=70,linesty=2
     legend_1,['Silicon','InGaAs'],textcolor=[255,70],chars=2
     legend_1,[strjoin(tempx[*,2])+' deg',strjoin(tempx[*,3])+' deg'],textcolor=[255,70],/right
     !P.multi=0
     p=tvrd(true=1)
    ; write_png,path+date+l+'_irradiance.png',p
  endif
  ; +----------------------------------------------------------
  ;stop
  ; +----------------------------------------------------------
  ;  (4) Process secondary calibration (only one at a time)
  bn    = 200 ; max # of spectra in a file
  spec  = {btime: lonarr(2), bcdtimstp:  bytarr(12),intime1: long(0),$
           intime2: long(0), intime3: long(0), intime4: long(0),accum: long(0), shsw:long(0),$
           zsit: ulong(0),nsit: ulong(0),zirt: ulong(0),nirt: ulong(0), zirx: ulong(0), nirx: ulong(0),xt: ulong(0),it: ulong(0),$
           zspecsi: intarr(np), zspecir: intarr(np), nspecsi: intarr(np), nspecir: intarr(np)}
  spc   = fltarr(2,2,2,np)     ; spectra (nadir/zenith,Silicon/InGaAs,dark/cal,# channels)
  tem   = fltarr(2,2,2)        ; temperatures of relevant spectrometers
  adj   = fltarr(2)            ; adjustment factor for changing integration times (Silicons only)
  rl    = ['Silicon','InGaAs'] ; spectrometer label
  for i=0,1 do begin           ; 0:nadir, 1:zenith
    for j=0,1 do begin           ; 0:Silicon, 1:InGaAs
      if i eq 0 then begin ; nadir
        dir='nadir'
        chk=donadir
        if j eq 0 then begin ; Silicon
          rad=secondary_nadir_si
          int=fix(inttime_nadir_si)
        endif else begin     ; InGaAs
          rad=secondary_nadir_ir
          int=fix(inttime_nadir_ir)
        endelse
      endif else begin     ; zenith
        dir='zenith'
        chk=dozenith
        if j eq 0 then begin ; Silicon
          rad=secondary_zenith_si
          int=fix(inttime_zenith_si)
        endif else begin     ; InGaAs
          rad=secondary_zenith_ir
          int=fix(inttime_zenith_ir)
        endelse
      endelse

      if(strcmp(chk,'yes')) then begin ; check if this direction (zenith/nadir) should be processed
        for k=0,1 do begin                          ; 0:dark, 1:cal
          if k eq 0 then file=rad+'dark'+l else file=rad+'cal'+l
          spc_files = file_search(file+data, count = numfiles, /FOLD_CASE)
          if (numfiles ne 1) then print,'Warning: There should be exactly 1 file for the dark.'
          openr,lunn,spc_files[0],/get_lun
          ctn=0L
          spt  = fltarr(np,bn) ; spectra time series
          t    = fltarr(bn)    ; temperature time series
          while not eof(lunn) do begin
            readu, lunn, spec
            if i eq 0 then begin   ; nadir
              if j eq 0 then begin ; Silicon
                dt =spec.intime3
                sp0=spec.nspecsi
                ch1 = ((spec.nsit/2048.)*5.) - 5.   ; Nadir Si spect temp.
                nsit = long(ch1 * 10000.)     ;convert to integer for use as index
                if (nsit le 19999) then t0 = revts[nsit]       ;nadir si temp
              endif else begin     ; InGaAs
                dt =spec.intime4
                sp0=spec.nspecir
                ch7=((spec.nirx/2048.)*5.) - 5. & rtem = abs((2000. * ch7)/(1. - (0.2 * ch7))) &  dem = 1.0295e-3+(2.391e-4 * alog(rtem)) + (1.568e-7 * (alog(rtem))^3)
                t0 = float(1./dem - 273.) ; temperature
              endelse
            endif else begin       ; zenith
              if j eq 0 then begin ; Silicon
                dt =spec.intime1
                sp0=spec.zspecsi
                ch0 = ((spec.zsit/2048.)*5.) - 5.   ; Zenith SI spect temp.
                zsit = long(ch0 * 10000.)     ;convert to integer for use as index
                if (zsit le 19999) then t0 = revts[zsit]       ;load zenith temp into array
              endif else begin     ; InGaAs
                dt =spec.intime2
                sp0=spec.zspecir
                ch7=((spec.zirx/2048.)*5.) - 5. & rtem = abs((2000. * ch7)/(1. - (0.2 * ch7))) & dem = 1.0295e-3+(2.391e-4 * alog(rtem)) + (1.568e-7 * (alog(rtem))^3)
                t0 = float(1./dem - 273.) ; temperature
              endelse
            endelse
            if dt ne int and j eq 1 then message,'InGaAs integration times do not match '+dir+rl[j]
            ;if dt ne int and j eq 0 then print,'Silicon integration times do not match '+dir+rl[j]
            spt[*,ctn]=sp0
            t  [  ctn]=t0
            ctn = ctn + 1L
          endwhile
          free_lun,lunn
          for ch=0,np-1 do begin
            spc[i,j,k,ch]=mean(spt[ch,0:ctn-1])
          endfor
          tem[i,j,k]=mean(t[0:ctn-1])
        endfor                  ; k loop (dark/cal)
        if j eq 0 then begin
          adj[i]=float(int)/float(dt)
        endif
        ; check date
        atime  = systime(0, spec.btime[0], /utc) ; convert date
        result = strpos(atime(0), ':', 0) ; find first incidence of ':'
        day1 =  strmid(atime(0), result - 5, 1) &  day2 =  strmid(atime(0), result - 4, 1)
        if(day1 eq ' ') then day = '0' + day2 else  day = day1 + day2
        mon =  strmid(atime(0), result - 9, 3) & mon = strtrim(string(where(months eq mon) + 1),1)
        if(mon lt 10) then mon = '0' + string(mon[0]) else mon = string(mon[0])
        year = fix(strmid(atime(0), result + 7, 4)) ;get year
        date = strtrim(string(year,mon,day),1)
        if not strcmp(primary_cal_date,date,8) then print,'Warning: Transfer cal date incorrect:'+primary_cal_date+' '+date
      endif ; do direction (nadir/zenith)
    endfor  ; j loop (Silicon/InGaAs)
  endfor  ; i loop (nadir/zenith)

  ; calculate secondary response functions
  ; 0 - zen si, 1 - zen ir, 2 - nad si, 3 - nad ir
  resp2=fltarr(np,4)
  temp2=fltarr(2,4)  ; 2: cal&dark

  print,'Silicon integration time adjustment [nadir; zenith]',adj
  resp2[*,0] = (spc[1,0,1,*]-spc[1,0,0,*])*adj[1]/float(inttime_zenith_si)/(lamp2[*,0])
  resp2[*,1] = reverse(reform(spc[1,1,1,*]-spc[1,1,0,*]))/float(inttime_zenith_ir)/(lamp2[*,1])
  resp2[*,2] = (spc[0,0,1,*]-spc[0,0,0,*])*adj[0]/float(inttime_nadir_si)/(lamp2[*,2])
  resp2[*,3] = reverse(reform(spc[0,1,1,*]-spc[0,1,0,*]))/float(inttime_nadir_ir)/(lamp2[*,3])
  temp2[*,0] = tem[1,0,*]
  temp2[*,1] = tem[1,1,*]
  temp2[*,2] = tem[0,0,*]
  temp2[*,3] = tem[0,1,*]

  ; save response functions
  if 1 then begin
    if (strcmp(donadir,'yes')) then begin
      resp2_si_file = rpath + secondary_cal_date + '_'+secondary_lamp+'_resp2_'+strcompress(string(inttime_nadir_si),/REMOVE_ALL)+'_nadsi.dat'
      resp2_ir_file = rpath + secondary_cal_date + '_'+secondary_lamp+'_resp2_'+strcompress(string(inttime_nadir_ir),/REMOVE_ALL)+'_nadir.dat'
      openw,us,resp2_si_file,/get_lun
      openw,ui,resp2_ir_file,/get_lun
      for i=0,np-1 do begin
        printf,us,wlvisn[i],resp2[i,2]
        printf,ui,wlnirn[i],resp2[i,3]
      endfor
      free_lun,us
      free_lun,ui
    endif

    if (strcmp(dozenith,'yes')) then begin
      resp2_si_file = rpath + secondary_cal_date + '_'+secondary_lamp+'_resp2_'+strcompress(string(inttime_zenith_si),/REMOVE_ALL)+'_zensi.dat'
      resp2_ir_file = rpath + secondary_cal_date + '_'+secondary_lamp+'_resp2_'+strcompress(string(inttime_zenith_ir),/REMOVE_ALL)+'_zenir.dat'
      openw,us,resp2_si_file,/get_lun
      openw,ui,resp2_ir_file,/get_lun
      for i=0,np-1 do begin
        printf,us,wlvisz[i],resp2[i,0]
        printf,ui,wlnirz[i],resp2[i,1]
      endfor
      free_lun,us
      free_lun,ui
    endif
    resp2_save = rpath + secondary_cal_date + '_'+secondary_lamp+'_resp2.sav'
    save,file=resp2_save,resp2,temp2,inttime_zenith_si,inttime_zenith_ir,inttime_nadir_si,inttime_nadir_ir
  endif


  ; make plots
  if plt then begin
    !P.multi=[0,1,2]
    window,3,retain=2,xs=700,ys=800
    plot,wlvisz,resp2[*,0],xtit='Wavelength [nm]',ytit='DN/ms / W/m2/nm',tit='Secondary Response Functions (Silicon)',chars=2,xr=[300,1200],/xs,yr=[0,300]
    oplot,wlvisn,resp2[*,2],color=120
    oplot,wlvisz,resp1[*,0],linesty=2,thick=2 ; primary for reference
    oplot,wlvisn,resp1[*,2],color=120,linesty=2,thick=2
    legend_1,['zenith','nadir'],textcolor=[255,120]
    legend_1,[secondary_cal_date,primary_cal_date],linesty=[0,2],/right
    ;window,1,retain=2,xs=700,ys=500
    plot,wlnirz,resp2[*,1],xtit='Wavelength [nm]',ytit='DN/ms / W/m2/nm',tit='Secondary Response Functions (InGaAs)',chars=2,xr=[900,2200],/xs,yr=[0,300]
    oplot,wlnirn,resp2[*,3],color=120
    oplot,wlnirz,resp1[*,1],linesty=2,thick=2 ; primary for reference
    oplot,wlnirn,resp1[*,3],color=120,linesty=2,thick=2
    legend_1,['zenith','nadir'],textcolor=[255,120]
    legend_1,[secondary_cal_date,primary_cal_date],linesty=[0,2],/right
    !P.multi=0
    p=tvrd(true=1)
    ;write_png,path+date+l+secondary_lamp+'_response2.png',p
  endif
  ; +----------------------------------------------------------

  stop
end
