@sub_cfg.pro
@legend_1.pro
@read_plt3.pro
@get_cos_wl.pro
@sub_zensun.pro
@sub_rph2za.pro
@sub_muslope.pro
@read_hsk_p3.pro
@time_sync.pro

pro rad

  ;++++++++++++++++++++++++++++++++++++++++++++
  ; settings
  ;--------------------------------------------
  date='20160920'
  dir ='/Users/sabrinacochrane/data/oracles/p3/'
  ll  ='/'
  sync=0 ; synchronize data sets (nav<-OSA2<-plt3)
  plot_map=0
  sf  =1                        ; scale factor (should be 1.0 if calibration is not too far off)
  coord=0 ;do we have coordination with er2?(and was HSRL working?)
  ;--------------------------------------------

  ;++++++++++++++++++++++++++++++++++++++++++++
  ; get information from configuration file
  ;--------------------------------------------
  ; initialize configuration file
  cfg_file=dir+ll+date+ll+date+'.cfg'        ; build cfg file
  if file_test(cfg_file) ne 1 then message,'Did not find configuration file.'
  plot=cfg(cfg_file,'plot')
  ; get platform
  if strcmp(strmid(plot,0,1),'y',/FOLD_CASE) then plot=1 else plot=0
  if plot then begin
    device, decomposed=0
    loadct,27
    AAA = FINDGEN(17) * (!PI*2/16.)
    USERSYM, COS(AAA), SIN(AAA), /FILL
  endif
  platform=cfg(cfg_file,'platform')
  print,'Process flight from '+date+' onboard the '+platform+'.'

  if strcmp(platform,'P-3',1) then p3=1 else p3=0
  ds=cfg(cfg_file, 'ds')
  if p3 then dp=cfg(cfg_file, 'dp')

  c0=cfg(cfg_file,'comment')
  if not strcmp(c0,'#') then begin
    print,c0
    comment=c0
  endif
  ; get extraterrestial flux
  extra=cfg(cfg_file,'extra') ; extra-terrestial flux
  ; get angular offsets for ER-2
  if p3 then begin
    ; LabVIEW
    pitso=cfg(cfg_file,'pitso')  ; pitch offset SPAN
    rolso=cfg(cfg_file,'rolso')  ; roll offset  SPAN
    pitao=cfg(cfg_file,'pitao')  ; pitch offset ARINC
    rolao=cfg(cfg_file,'rolao')  ; roll offset  ARINC
    ; reality
    p0=cfg(cfg_file,'pos')  ; pitch offset SPAN
    r0=cfg(cfg_file,'ros')  ; roll offset  SPAN
    p1=cfg(cfg_file,'poa')  ; pitch offset ARINC
    r1=cfg(cfg_file,'roa')  ; roll offset  ARINC
  endif else begin
    p0=cfg(cfg_file,'pos')  ; pitch offset ER-2 INS
    r0=cfg(cfg_file,'ros')  ; roll offset  ER-2 INS
  endelse

  ; get time range
  i0=cfg(cfg_file,'interval') ; look if we should only use data within a certain time window
  uu0=0
  if not strcmp(i0,'#') then begin
    uu=strsplit(i0,' ,',escape='#',/EXTRACT)
    uu=float(uu)
    uu0=1
    u0=uu[0] & u1=uu[1]
    rux=[u0,u1]
  endif
  wl=cfg(cfg_file,'wl')
  wl=strsplit(wl,' ,',escape='#',/EXTRACT)
  wl=float(wl)
  nl=n_elements(wl)
  ; get TOA irradiance
  openr,10,extra
  wlk=fltarr(2001) & flk=fltarr(2001)
  for i=250,2250 do begin
    readf,10,a,b
    wlk[i-250]=a & flk[i-250]=b
  endfor
  close,10
  flk=smooth(flk,10)
  ;--------------------------------------------

  ;++++++++++++++++++++++++++++++++++++++++++++
  ; get HSK data
  ;--------------------------------------------
  if 1 then begin
  hsk=dir+date+ll+'Hskping_P3_'+date+'_R0.ict'
  read_hsk_p3,hsk,date,utc_hsk,lat,lon,alt,palt,gsp,asp,isp,vsp,pit,rol,hed,sza,tst,pst,mr,pv,rh,wsp,wdi,asn,saz,aza
  filt=where(utc_hsk ne -9999 and lat ne -9999 and lon ne -9999 and alt ne -9999 $
             and rol ne -9999 and hed ne -9999 and pit ne -9999 and sza ne -9999 and aza ne -9999)
  utc=utc_hsk[filt] & lat=lat[filt] & lon=lon[filt] & alt=alt[filt] & pit=pit[filt]$
      & rol=rol[filt] & hed=hed[filt] & sza=sza[filt]  &aza=aza[filt] &asn=asn[filt] & gsp=gsp[filt]
  endif

  ;--------------------------------------------

  ;++++++++++++++++++++++++++++++++++++++++++++
  ; get PLT data
  ;--------------------------------------------
  if strcmp(platform,'P',1) then begin
    smooth=10 & dpa=3.8 & dra=1.1
    read_plt3,dir+date+ll,utc_plt,pits,pita,pitm,piti,rols,rola,rolm,roli,alts,stas,$
              smooth=smooth,dpa=dpa,dra=dra
    utc_plt=utc_plt+dp
  endif

  ;++++++++++++++++++++++++++++++++++++++++++++
  ; restore SSFR data
  ;--------------------------------------------
  restore, dir+date+ll+date+'_calibspcs.out'
  if n_elements(a24) eq 1 then begin
    flt=where(tmhrs lt a24,n24)
    if n24 gt 0 then tmhrs[flt]=tmhrs[flt]+24.
  endif
  utc_ssfr=tmhrs-ds
  wl=zenlambda
  nl=n_elements(wl)
  ;--------------------------------------------


  ;++++++++++++++++++++++++++++++++++++++++++++
  ; interpolate SSFR (and ALP) data to HSK time base
  ;--------------------------------------------
  nt=n_elements(utc)
  nzen=n_elements(zenlambda) & nnad=n_elements(nadlambda)
  nind=intarr(nl) & zind=intarr(nl)
  nad=fltarr(nl,nt)
  zen=fltarr(nl,nt)
  toa=fltarr(nl)
  if sync then begin
    time_sync,utc, utc_ssfr, utc_plt, pits, pitm,pita,piti, pit, zspectra, asn
  endif
  nln=n_elements(nadlambda)
  nlz=n_elements(zenlambda)
  nad1=fltarr(nln, nt)
  zen1=fltarr(nlz, nt)
  for i=0, nlz-1 do begin
    nad1[i,*]=interpol(nspectra[i,*],utc_ssfr,utc)
    zen1[i,*]=interpol(zspectra[i,*],utc_ssfr,utc)
  endfor
  toa=interpol(flk,wlk,wl)*0.001
  if strcmp(platform,'P',1) then begin
    pits=interpol(pits,utc_plt,utc)
    pita=interpol(pita,utc_plt,utc)
    piti=interpol(piti,utc_plt,utc)
    pitm=interpol(pitm,utc_plt,utc)
    rols=interpol(rols,utc_plt,utc)
    rola=interpol(rola,utc_plt,utc)
    roli=interpol(roli,utc_plt,utc)
    rolm=interpol(rolm,utc_plt,utc)
    sta =interpol(stas,utc_plt,utc)
    alt =interpol(alts,utc_plt,utc)
  endif
  ;--------------------------------------------

  ;++++++++++++++++++++++++++++++++++++++++++++
  ; Cosine Correction
  ;--------------------------------------------
  cos_zen=cfg(cfg_file, 'coszen')
  cos_nad=cfg(cfg_file, 'cosnad')

  juld=julian_day(strmid(date,0,4),strmid(date,4,2),strmid(date,6,2))
  zensun, juld,utc,lat,lon,sza,azimuth,solfac
  mu=cos(sza*!pi/180.)
  dc=fltarr(nt)

  ; first determine which INS we are running on
  lev=intarr(nt)
  flts=where(abs(rols-rolso-rolm) lt .1 and abs(pits-pitso-pitm) lt .1,ns)
  lev[flts]=+1 ; span-cpt
  flta=where(abs(rola-rolao-rolm) lt .1 and abs(pita-pitao-pitm) lt .1,na)
  lev[flta]=-1 ; arinc
  ; not using dc, using mu because we have platform
  rph2za,pits-pitso-p0-pitm,rols-rolso-r0-rolm,hed,ssfrzenith_s,ssfrazimuth_s
  rph2za,pita-pitao-p1-pitm,rola-rolao-r1-rolm,hed,ssfrzenith_a,ssfrazimuth_a

  dc[flts]=muslope(sza[flts],azimuth[flts],ssfrzenith_s[flts],ssfrazimuth_s[flts])
  dc[flta]=muslope(sza[flta],azimuth[flta],ssfrzenith_a[flta],ssfrazimuth_a[flta])

  get_cos, cos_zen, cos_nad, sza,dc,zenlambda,facz,dz,nadlambda,facn,dn ,faczk,facnk
  zenspectra=fltarr(nlz,nt)
  nadspectra=fltarr(nln,nt)
  fc=where(dc gt 0.1) ; minimal mu
  for i=0,nlz-1 do begin
     zenspectra[i,fc]=zen1[i,fc]
     zenspectra[i,*]=zenspectra[i,*]*facz[*,i]
     nadspectra[i,fc]=nad1[i,fc]
     nadspectra[i,*]=nadspectra[i,*]*dn[i]; UPDATE THIS BECUASE ITS WRONG
  endfor

  ;--------------------------------------------
  if 0 then begin
    ;check corrected spectra
    window, 0, xs=1000, ys=700
    !p.multi=0
    plot, utc, zenspectra[54,*], color=30, psym=4
    oplot, utc, zen1[54,*], color=120, psym=3
    oplot, utc, nadspectra[54,*], color=90, psym=3
    oplot, utc, nad1[54,*],color=120, psym=3
    oplot, utc, facz[*,54]*1.5, color=200, psym=3
    legend_1, ['Z Cor all', 'Z Cor flt', 'Z uncor', 'N cor', 'N uncor', 'FACZ'], textcolor=[30,120,90,120,200]
    cursor, x, y
    oplot, [x,x],[0,5], linestyle=2
  endif
  ;--------------------------------------------

  ;++++++++++++++++++++++++++++++++++++++++++++
  ; plot time series
  ;--------------------------------------------
  nm=54 ;532 nm

  window,1,retain=2
  !P.multi=0
  cs=2
  if strcmp(platform,'P',1) then begin ; P3 processing
     flt=where(abs(lev) gt 0)
     ref=cos(!pi/180.*(sza))
  endif
  plot,utc[flt],zenspectra[nm,flt],xtit='UTC [h]',ytit='Irradiance [W m-2 nm-1]',psym=4,$
       chars=cs,title=date+' '+platform+' '+strcompress(string(zenlambda[nm]),/remove_all) +'nm',$
       xr=[u0,u1],/xs,syms=0.5,yr=[0,max(zspectra[nm,*])], color=255
  oplot,tmhrs,zspectra[nm,*],color=30, psym=3
  oplot,utc[flt],ref[flt]*toa[nm]*sf,color=120
  oplot,utc[flt],nadspectra[nm,flt],color=70,psym=3
  oplot, utc[flt], alt[flt]/3500, color=90
  legend_1,['zenith c','zenith uc','toa','nadir', 'alt/3500'],textcolor=[255,30,120,70,90, 60]


  outfile=dir+'/'+date+'/'+date+'_calibspecs_attcorr.out'
  save, file=outfile, tmhrs, zenlambda, zspectra, nadlambda, nspectra

end
