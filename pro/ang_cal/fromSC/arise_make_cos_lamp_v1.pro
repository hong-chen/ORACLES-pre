;@read_sks10_bruce.pro
@read_sks6_bruce.pro
@arise_read_cos_bruce_lamp.pro
@fit_cos.pro

; Comments:
; In this case, the difference between the "minus" and "plus"
; direction is used as uncertainty of the cosine response, but
; other factors are diffuse vs. direct illumination during the
; calibration. May switch to diffuser in the future.
; For ARISE, Hong will use this code to process the cosine response (diode/lamp)
; For ORACLES, Sabrina will use this code to process the cosine
; response.

; TBD:
; So far, we don't get uncertainties, but these can be derived
; from the difference between the "minus" and "plus" run (swiveling
; light collector in both directions to measure cosine response / symmetry).

pro make_cos
    ; +----------------------------------------------------------
    ; User setttings
    ; Use sks10_bruce for nadir and sks6_bruce for zenith - who knows why...
    path='/Users/sabrinacochrane/data/oracles/p3/ang/' ; change as needed
    lab='ZENITH'
    if strcmp(lab,'ZENITH') then begin
        v6=1
        date='20150219'
    endif else begin
        date='20150220'
    endelse
    off = 0.02                         ; offset (below which response won't be valid - direct only)
    ch1 = 108                          ; channel Si to be displayed
    ch2 = 108                          ; channel NIR to be displayed
    ; +----------------------------------------------------------

    ; settings
    device,decomposed=0
    loadct,27

    ; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ; get data
    ; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    read_cos,path,date,mu,ang,wlvis,wlnir,cmv,cmn,cpv,cpn,v6=v6 ; (cosine +/-, vis/nir)
    ; comment: two directions were measured in this case

    ; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ; plot response
    ; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    window,0,title='raw data',retain=2
    plot ,mu,cmv[ch1,*]/cmv[ch1,0],psym=2,xtit='cos(ang)',ytit='response',chars=2,tit=date+' '+lab
    oplot,mu,cpv[ch1,*]/cpv[ch1,0],psym=4
    ;oplot,mu,cmn[ch2,*]/cmn[ch2,0],psym=2,color=120
    ;oplot,mu,cpn[ch2,*]/cpn[ch2,0],psym=4,color=120
    oplot,[0,1],[0,1],linesty=2
    ;legend,['  -','  +'],psym=[2,4],chars=2

    nm=n_elements(mu)
    window,1,xs=800,ys=600,title='wavelength fitting for each mu'
    dg=7                          ; degree of fit polynomial
    nf=1771
    wlf=indgen(nf)+350.

    rsf=fltarr(nf,nm)
    fv=where(wlvis ge min(wlf) and wlvis lt 900)   ; adjust if needed
    fn=where(wlnir gt 1000 and wlnir le max(wlf)) ; adjust if needed

    m0=0 ; reference mu (usually 0, but can be final measurement as well, if also mu=1)
    rsf[*,m0]=1.
    for m=1,nm-1 do begin
        c0=mean(cmv[*,m])/mean(cmv[*,m0])
        yr=[c0-0.2,c0+0.2]
        if yr[0] lt 0 then yr[0]=0
        plot,wlvis,cmv[*,m]/cmv[*,m0],xr=[min(wlf),max(wlf)],thick=2,/xs,yr=yr,/ys,xtitle='Wavelength [nm]',ytitle='Cosine Response',chars=2,title=lab+' mu='+string(mu[m])
        oplot,wlnir,cmn[*,m]/cmn[*,m0],thick=2,linesty=2
        oplot,wlvis,cpv[*,m]/cpv[*,m0],thick=2,color=120
        oplot,wlnir,cpn[*,m]/cpn[*,m0],thick=2,linesty=2,color=120
        ;res=poly_fit([wlvis[fv]],[0.5*(cmv[fv,m]/cmv[fv,m0]+cpv[fv,m]/cpv[fv,m0])],dg)
        res=poly_fit([wlvis[fv],wlnir[fn]],[0.5*(cmv[fv,m]/cmv[fv,m0]+cpv[fv,m]/cpv[fv,m0]),0.5*(cmn[fn,m]/cmn[fn,m0]+cpn[fn,m]/cpn[fn,m0])],dg)
        for i=0,dg do rsf[*,m]=rsf[*,m]+res[i]*wlf^i
        oplot,wlf,rsf[*,m],color=30,thick=2
        oplot,[350,2150],[mu[m],mu[m]],linesty=2,color=30
        wait,1
    endfor

    window,2,title='fitted response overview'
    plot,wlf,rsf[*,1],xr=[min(wlf),max(wlf)],yr=[0,1.2],title=lab,/xs,xtitle='Wavelength [nm]',ytitle='Response',chars=2
    for m=2,nm-1 do begin
        oplot,wlf,rsf[*,m],color=m*20
        oplot,[350,2150],[mu[m],mu[m]],linesty=2,color=m*20
    endfor

    ; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ; Do mu fitting for all wavelengths
    ; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    lo = 0.70   ; linear fit for mu<lo
    hi = 0.85   ; polynomial fit for mu>hi

    window,3,title='fitted mus'
    df=fltarr(nf)                 ; diffuse correction
    dr=fltarr(nf,1001)-999.       ; direct correction (initialized with -999)
    for l=0,nf-1 do begin
        flt=where(rsf[l,*] gt off)
        fit_cos,mu[flt],rsf[l,flt],lo,hi,mu_grd,sp_grd ;,max_error=1.5;,/verbose
        flt=where(sp_grd gt 0)
        df[l]=0.5/int_tabulated(mu_grd[flt],sp_grd[flt],/double) ; diffuse correction

        plot,mu,rsf[l,*],psym=4,title=lab+string(wlf[l]),yr=[0,1.2],xtitle='MU',ytitle='Response',chars=2
        flt=where(sp_grd gt off,n0)
        oplot,mu_grd[flt],sp_grd[flt]
        dr[l,flt]=mu_grd[flt]/sp_grd[flt]

        oplot,[lo,lo],[0,1.2],linesty=2
        oplot,[hi,hi],[0,1.2],linesty=2
        oplot,[0,1],[0,1],linesty=2

        wait,0.02
    endfor

    ; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ; Checking of results an save file
    ; +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    window,4,title='check results wl'
    plot,wlf,df,xtitle='Wavelength [nm]',ytitle='Response',chars=2,xr=[min(wlf),max(wlf)],/xs
    oplot,wlf,dr[*,500],color=120

    mu=mu_grd
    save,file=path+'/'+lab+'.idl',wlf,mu,df,dr,path,date,lab,lo,hi

    STOP

end
