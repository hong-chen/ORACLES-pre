pro read_cos,path,date,mu,ang,wlvisz,wlnirz,cosineminusvis,cosineminusnir,cosineplusvis,cosineplusnir,v6=v6

    ; (1) make wavelengths
    np=256
    ;# Si Zenith 062762
    zlambdasi=' 301.946 3.31877 0.00037585 -1.76779E-6 0'
    ;# InGaAs Zenith 070088
    zlambdair=' 2202.33 -4.35275 -0.00269498 3.84968e-6 -2.33845e-8'
    ;# Si Nadir 071332
    nlambdasi='302.818 3.31912 0.000343831 -1.81135e-6 0'
    ;# InGaAs Nadir 083029
    nlambdair='2210.29 -4.5998 0.00102444 -1.60349E-5 1.29122E-8'

    ; wavelengths
    wlvisz=fltarr(np) & wlnirz=fltarr(np)
    wlvisn=fltarr(np) & wlnirn=fltarr(np)

    ; make wavelengths / zenith
    zlambdasi=strsplit(zlambdasi,' ,',count=znlsi,escape='#',/EXTRACT) ; make string into substrings
    zlambdasi=float(zlambdasi)
    zlambdair=strsplit(zlambdair,' ,',count=znlir,escape='#',/EXTRACT) ; make string into substrings
    zlambdair=float(zlambdair)

    wlvisz=fltarr(np) & wlnirz=fltarr(np)
    for i=0,np-1 do begin
        for j=0,znlsi-1 do wlvisz[i]     =wlvisz[i]     +zlambdasi[j]*float(i)^j
        for j=0,znlir-1 do wlnirz[np-1-i]=wlnirz[np-1-i]+zlambdair[j]*float(i)^j
    endfor

    ; make wavelengths / nadir
    nlambdasi=strsplit(nlambdasi,' ,',count=nnlsi,escape='#',/EXTRACT) ; make string into substrings
    nlambdasi=float(nlambdasi)
    nlambdair=strsplit(nlambdair,' ,',count=nnlir,escape='#',/EXTRACT) ; make string into substrings
    nlambdair=float(nlambdair)
    wlvisn=fltarr(np) & wlnirn=fltarr(np)
    for i=0,np-1 do begin
        for j=0,nnlsi-1 do wlvisn[i]     =wlvisn[i]     +nlambdasi[j]*float(i)^j
        for j=0,nnlir-1 do wlnirn[np-1-i]=wlnirn[np-1-i]+nlambdair[j]*float(i)^j
    endfor

    ; (2) read all the measurments
    files=findfile(path+date+'*.SKS',count=numspectra)
    vis=fltarr(256,numspectra)
    nir=fltarr(256,numspectra)
    for i=0,numspectra-1 do begin
        if n_elements(v6) eq 1 then begin
            print,'v6'
            read_SKS,files[i],spect, eos, shutter, inttime, temps, UTC, count,ndarks,darks,comment
            tmp=mean(spect[*,*,2],dimension=1)-mean(darks[*,*,2],dimension=1)
            vis[*,i]=tmp[1:256]
            tmp=mean(spect[*,*,3],dimension=1)-mean(darks[*,*,3],dimension=1)
            nir[*,i]=tmp[1:256]
        endif else begin
            print,'v10'
            read_SKS,files[i],count, eos, shutter, inttime, spect, spect_temps, spect_UTC, $
                spect_DATE, spect_ind, specst, speced,darks, dark_temps, dark_UTC,    $
                dark_DATE, dark_ind, darkst, darked
            tmp=mean(spect[*,*,2],dimension=1)-mean(darks[*,*,2],dimension=1)
            vis[*,i]=tmp[1:256]
            tmp=mean(spect[*,*,3],dimension=1)-mean(darks[*,*,3],dimension=1)
            nir[*,i]=tmp[1:256]
        endelse
    endfor

    ;angles=[90.,87.5,85.,82.5,80.,77.5,75.,72.5,70.,65,60,55,50,45,40,35,30,25,20,15,10,5,0,$
    ;        -90,-87.5,-85.0,-82.5,-80.0,-77.5,-75.0,-72.5,-70,-65,-60,-55,-50,-45,-40,-35,-30,$
                                ;        -25,-20,-15,-10,-5,0]
    angles=[90.,87.5,85.,82.5,80.,77.5,75.,72.5,70.,65,60,55,50,45,40,35,30,25,20,15,10,5,0,$
            -5,-10,-15,-20,-25,-30,-35,-40,-45,-50,-55,-60,-65,-70,-72.5,-75,-77.5,-80,-82.5,-85,-87.5,-90,0]
    mu=cos(angles*!dtor)
    mup=[0:22]
    mum=[23:45]

    s=reverse(sort(mu[mup]))
    cosineplusvis=vis[*,mup[s]]
    cosineplusnir=nir[*,mup[s]]
    mup=mu[mup[s]]

    s=reverse(sort(mu[mum]))
    cosineminusvis=vis[*,mum[s]]
    cosineminusnir=nir[*,mum[s]]
    mum=mu[mum[s]]

    mu=mup

    return

    STOP

    ang = [0,5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,0]
    mu  = cos(ang*!dtor)
end
