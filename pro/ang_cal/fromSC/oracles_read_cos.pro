@oracles_read_osa2 ; for angular calibration use only
@oracles_read_sks  ; for angular calibration use only

;pro read_cos,path,date,mu,ang,wlvisz,wlnirz,cosineminusvis,cosineminusnir,cosineplusvis,cosineplusnir,v6=v6
pro read_cos, fnames, ang, index_cut, tag_ftype, tag_dir, mu, wlvisz, wlnirz, cosineminusvis, cosineminusnir, cosineplusvis, cosineplusnir


  np = 256

  if tag_dir eq 'zenith' then begin
    index_vis = 0
    index_nir = 1
  endif

  if tag_dir eq 'nadir' then begin
    index_vis = 2
    index_nir = 3
  endif

  if tag_ftype eq 'SKS' then begin
    ;# CU SSFR
    ;# Si Zenith 062762
    zlambdasi=' 301.946 3.31877 0.00037585 -1.76779E-6 0'
    ;# InGaAs Zenith 070088
    zlambdair=' 2202.33 -4.35275 -0.00269498 3.84968e-6 -2.33845e-8'
    ;# Si Nadir 071332
    nlambdasi='302.818 3.31912 0.000343831 -1.81135e-6 0'
    ;# InGaAs Nadir 083029
    nlambdair='2210.29 -4.5998 0.00102444 -1.60349E-5 1.29122E-8'
  endif

  if tag_ftype eq 'OSA2' then begin
    ;# SSFR6
    ;# Si Zenith 033161
    zlambdasi='303.087 3.30588 4.09568E-04 -1.63269E-06 0'
    ;# InGaAs Zenith 044832
    zlambdair='2213.37 -4.46844 -0.00111879 -2.76593E-06 -1.57883E-08'
    ;# Si Nadir 045924
    nlambdasi='302.255 3.30977 4.38733E-04 -1.90935E-06 0'
    ;# InGaAs Nadir 044829
    nlambdair='2225.74 -4.37926 -0.00220588 2.80201E-06 -2.2624E-08'
  endif

  ; wavelengths
  wlvisz=fltarr(np) & wlnirz=fltarr(np)
  wlvisn=fltarr(np) & wlnirn=fltarr(np)

  ; make wavelengths / zenith
  zlambdasi=strsplit(zlambdasi,' ,',count=znlsi,escape='#',/EXTRACT) ; make string into substrings
  zlambdasi=float(zlambdasi)
  zlambdair=strsplit(zlambdair,' ,',count=znlir,escape='#',/EXTRACT) ; make string into substrings
  zlambdair=float(zlambdair)
  wlvisz=fltarr(np) & wlnirz=fltarr(np)
  for i=0,np-1 do begin
    for j=0,znlsi-1 do wlvisz[i]     =wlvisz[i]     +zlambdasi[j]*float(i)^j
    for j=0,znlir-1 do wlnirz[np-1-i]=wlnirz[np-1-i]+zlambdair[j]*float(i)^j
  endfor

  ; make wavelengths / nadir
  nlambdasi=strsplit(nlambdasi,' ,',count=nnlsi,escape='#',/EXTRACT) ; make string into substrings
  nlambdasi=float(nlambdasi)
  nlambdair=strsplit(nlambdair,' ,',count=nnlir,escape='#',/EXTRACT) ; make string into substrings
  nlambdair=float(nlambdair)
  wlvisn=fltarr(np) & wlnirn=fltarr(np)
  for i=0,np-1 do begin
    for j=0,nnlsi-1 do wlvisn[i]     =wlvisn[i]     +nlambdasi[j]*float(i)^j
    for j=0,nnlir-1 do wlnirn[np-1-i]=wlnirn[np-1-i]+nlambdair[j]*float(i)^j
  endfor

  ; (2) read all the measurments
  ;files=findfile(path+'*.'+tag_ftype,count=numspectra)
  numspectra=n_elements(fnames)

  vis=fltarr(256,numspectra)
  nir=fltarr(256,numspectra)
  if tag_ftype eq 'SKS' then begin
     ;fname='/Users/sabrinacochrane/data/oracles/p3/ang/from_bruce/ORACLES_P3_LC1_NEWSPHERE/2TRY/DARKS/2016042700005.SKS'
     ;read_sks, fname, spec_raw_dark, shsw_raw_dark
  endif 

  for i = 0, numspectra-1 do begin

    if tag_ftype eq 'OSA2' then begin
       read_osa2, fnames[i], spec_raw, shsw_raw
           index_dark = where(shsw_raw eq 1)
           index_light= where(shsw_raw eq 0)
           for j = 0, 255 do begin
              vis[j, i] = mean(spec_raw[j, index_vis, index_light]) - mean(spec_raw[j, index_vis, index_dark])
              nir[j, i] = mean(spec_raw[j, index_nir, index_light]) - mean(spec_raw[j, index_nir, index_dark])
           endfor
    endif

    if tag_ftype eq 'SKS' then begin
       read_sks, fnames[i], spec_raw, shsw_raw
       shsw_raw=shsw_raw[index_vis,*]
       ;print, shsw_raw[*,3]
                                
       ;index_dark = where(shsw_raw_dark eq 1)
       ;index_light= where(shsw_raw eq 0)
       index_dark = where(shsw_raw eq 1)
       index_light= where(shsw_raw eq 0)
                      for j = 0, 255 do begin
              ;vis[j, i] = mean(spec_raw[j, index_vis, index_light]) - mean(spec_raw_dark[j, index_vis, index_dark])
              ;nir[j, i] = mean(spec_raw[j, index_nir, index_light]) - mean(spec_raw_dark[j, index_nir, index_dark])
              vis[j, i] = mean(spec_raw[j, index_vis, index_light]) - mean(spec_raw[j, index_vis, index_dark])
              nir[j, i] = mean(spec_raw[j, index_nir, index_light]) - mean(spec_raw[j, index_nir, index_dark])
              ;stop
           endfor         
    endif

   ; print, n_elements(shsw_raw)
   ; print, shsw_raw
   ; print, '---'


  endfor

  ;angles=[90.,87.5,85.,82.5,80.,77.5,75.,72.5,70.,65,60,55,50,45,40,35,30,25,20,15,10,5,0,$
  ;        -5,-10,-15,-20,-25,-30,-35,-40,-45,-50,-55,-60,-65,-70,-72.5,-75,-77.5,-80,-82.5,-85,-87.5,-90,0]

  na=n_elements(ang)
  mu=cos(ang*!dtor)
  mup=[0:index_cut]
  mum=[(index_cut+1):na]

  s=reverse(sort(mu[mup]))
  cosineplusvis=vis[*,mup[s]]
  cosineplusnir=nir[*,mup[s]]
  mup=mu[mup[s]]

  s=reverse(sort(mu[mum]))
  cosineminusvis=vis[*,mum[s]]
  cosineminusnir=nir[*,mum[s]]
  mum=mu[mum[s]]

  mu=mup

  return
end
