pro read_osa2, fname, spec_raw, shsw_raw

  np      = 256

  bignum  = 60000L ; defines maximum spectrum number - increase if needed

  spec_raw = fltarr(np, 4, bignum)
  shsw_raw = fltarr(bignum)

  spect = {btime: lonarr(2), bcdtimstp:  bytarr(12),intime1: long(0),$
           intime2: long(0), intime3: long(0), intime4: long(0),accum: long(0), shsw:long(0),$
           zsit: ulong(0),nsit: ulong(0),zirt: ulong(0),nirt: ulong(0), zirx: ulong(0), nirx: ulong(0),xt: ulong(0),it: ulong(0),$
           zspecsi: intarr(np), zspecir: intarr(np), nspecsi: intarr(np), nspecir: intarr(np)}

  openr, lun, fname, /get_lun
  count = 0
  while not eof(lun) do begin ; Read individual spectra

    readu, lun, spect
    spec_raw[*, 0, count] = spect.zspecsi
    spec_raw[*, 1, count] = spect.zspecir
    spec_raw[*, 2, count] = spect.nspecsi
    spec_raw[*, 3, count] = spect.nspecir
    shsw_raw[count]       = spect.shsw
    count = count + 1

  endwhile

  free_lun, lun

  spec_raw = spec_raw[*, *, 0:count-1]
  shsw_raw = shsw_raw[0:count-1]

end
