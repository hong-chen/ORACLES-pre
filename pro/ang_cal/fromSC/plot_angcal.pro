@legend_1.pro

pro plt

  device, decomposed=0
  loadct, 27

  p3=0
  er2=1
  
  if p3 then begin
  path='/Users/sabrinacochrane/data/oracles/p3/ang/'

  restore, path+'LC2_nadir_post.idl'
  df_post2=df
  dr_post2=dr
  mu_post2=mu
  wlf_post2=wlf
  restore, path+'LC2_nadir_pre.idl'
  df_pre2=df
  dr_pre2=dr
  mu_pre2=mu
  wlf_pre2=wlf
  restore,path+'cosine/ssfr6/20160805_lc2.out'
  mu_grd2=mu_grd
  sp_grd2=sp_grd

 plot, mu_pre2,mu_pre2/dr_pre2[316,*], xtitle='Mu', ytitle='Response', yrange=[0,1.2],charsize=2, title='LC2 666nm'
 oplot, mu_post2,mu_post2/dr_post2[316,*], color=30
 oplot, mu_grd2, sp_grd2, color=60
 legend_1, ['post mission', 'pre-mission reprocessed', 'pre-mission original'], textcolor=[30,255,60]

  restore, path+'LC1_zenith_post.idl'
  df_post1=df
  dr_post1=dr
  mu_post1=mu
  wlf_post1=wlf
  restore, path+'LC1_zenith_pre.idl'
  df_pre1=df
  dr_pre1=dr
  mu_pre1=mu
  wlf_pre1=wlf
  restore,path+'cosine/ssfr6/20160805_lC1.out'
  mu_grd1=mu_grd
  sp_grd1=sp_grd

  window, 1
 plot, mu_pre1,mu_pre1/dr_pre1[316,*], xtitle='Mu', ytitle='Response', yrange=[0,1.2],charsize=2, title='LC1 666nm'
 oplot, mu_post1,mu_post1/dr_post1[316,*], color=30
 oplot, mu_grd1, sp_grd1, color=60
 legend_1, ['post mission', 'pre-mission reprocessed', 'pre-mission original'], textcolor=[30,255,60]
endif


  if er2 then begin

  path='/Users/sabrinacochrane/data/oracles/p3/ang/'

  restore, path+'LC3_zenith_pre.idl'
  df_pre3=df
  dr_pre3=dr
  mu_pre3=mu
  wlf_pre3=wlf
  restore,path+'../../er2/ang/cosine/ssfr5/20160801_lC3.out'
  mu_grd3=mu_grd
  sp_grd3=sp_grd

  restore, path+'LC5_zenith_pre.idl'
  df_pre5=df
  dr_pre5=dr
  mu_pre5=mu
  wlf_pre5=wlf
  restore,path+'../../er2/ang/cosine/ssfr5/20160801_lC5.out'
  mu_grd5=mu_grd
  sp_grd5=sp_grd

    restore, path+'LC6_nadir_pre.idl'
  df_pre6=df
  dr_pre6=dr
  mu_pre6=mu
  wlf_pre6=wlf
  restore,path+'../../er2/ang/cosine/ssfr5/20160801_lC6.out'
  mu_grd6=mu_grd
  sp_grd6=sp_grd

window, 0
 plot, mu_pre3,mu_pre3/dr_pre3[316,*], xtitle='Mu', ytitle='Response', yrange=[0,1.2],charsize=2, title='LC3 666nm'
 oplot, mu_grd3, sp_grd3, color=60
 legend_1, [ 'pre-mission reprocessed', 'pre-mission original'], textcolor=[255,60]

window, 1
 plot, mu_pre5,mu_pre5/dr_pre5[316,*], xtitle='Mu', ytitle='Response', yrange=[0,1.2],charsize=2, title='LC5 666nm'
 oplot, mu_grd5, sp_grd5, color=60
 legend_1, [ 'pre-mission reprocessed', 'pre-mission original'], textcolor=[255,60]
 
 window, 2
 plot, mu_pre6,mu_pre6/dr_pre6[316,*], xtitle='Mu', ytitle='Response', yrange=[0,1.2],charsize=2, title='LC6 666nm'
 oplot, mu_grd6, sp_grd6, color=60
 legend_1, [ 'pre-mission reprocessed', 'pre-mission original'], textcolor=[255,60]
  
     
  endif 
  
  stop
end
