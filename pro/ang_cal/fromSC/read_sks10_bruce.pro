;+
; NAME:
;   read_SKS
;
; PURPOSE:
;   Program to read in the '*.SKS' binary files produced by SKS spectrometer system
;   ***********THIS VERSION WORKS FOR DATA COLLECTED AFTER 20140904*************
;
; CATEGORY:
;   SKS data reader
;
; CALLING SEQUENCE:
;  read_SKS, file, count, eos, shutter, inttime, spect, spect_temps, spect_UTC, spect_DATE, spect_ind, specst, speced, darks, dark_temps, dark_UTC, dark_DATE, dark_ind, darkst, darked
;   - file: file to be opened and read
;   - count: number of spectra in file (including darks)
;   - eos: end of signal check for each time and spectrometer
;   - shutter: shutter signal (opened=1, closed=0) for each spectrometer
;   - inttime: integration time for each spectrometer and time
;
;   - spect: array of all spectra (*not* including the darks) for all spectrometers
;            dimensions: (count-ndarks)x257x4
;            The first value in the second dimension is the spectrometer identifier. Therefore actual data are of (count-ndarks)x256x4.
;            The third dimension is the spectrometer dimension: 0 - Si Zenith, 1 - In Zenith, 2 - Si Nadir, 3 - In Nadir
;   - spect_temps: array of temperatures for data
;   - spect_UTC: time in UTC format (hours) for data
;   - spect_DATE: date of measurements in YYYYMMDD format for data
;
;   - darks: array of dark spectra (ndarks = 2+the actual number of darks saved; this is because we removed one dark spectra at each end)
;            dimensions: (ndarks-2)x257x4; Actual darks are of (ndarks-2)x256x4.
;   - dark_temps: array of temperatures for darks
;   - dark_UTC: time in UTC format (hours) for darks
;   - dark_DATE: date of measurements in YYYYMMDD format for darks
;
; OUTPUT:
;   arrays of measurements
;
; KEYWORDS:
;   none
;
; DEPENDENCIES:
;   none
;
; NEEDED FILES:
;   - *.SKS files
;
; EXAMPLE:
; IDL> file='*36.SKS'
; IDL> .r read_sks7.pro
; % Compiled module: READ_SKS.
; IDL> read_SKS, file, count, eos, shutter, inttime, spect, spect_temps, spect_UTC, spect_DATE, spect_ind, specst, speced, darks, dark_temps, dark_UTC, dark_DATE, dark_ind, darkst, darked
; IDL> help
;      % At $MAIN$
;      COUNT           INT       =      200
;      DARKED          LONG      = Array[7]
;      DARKS           INT       = Array[82, 257, 4]
;      DARKST          LONG      = Array[7]
;      DARK_DATE       STRING    = Array[82]
;      DARK_IND        LONG      = Array[82]
;      DARK_TEMPS      DOUBLE    = Array[82, 9]
;      DARK_UTC        DOUBLE    = Array[82]
;      EOS             BYTE      = Array[200, 4]
;      FILE            STRING    = '*01.SKS'
;      INTTIME         LONG      = Array[200, 4]
;      SHUTTER         BYTE      = Array[200]
;      SPECED          LONG      = Array[6]
;      SPECST          LONG      = Array[6]
;      SPECT           INT       = Array[78, 257, 4]
;      SPECT_DATE      STRING    = Array[78]
;      SPECT_TEMPS     DOUBLE    = Array[78, 9]
;      SPECT_UTC       DOUBLE    = Array[78]
;      spect_ind        LONG      = Array[78]
;      Compiled Procedures:
;          $MAIN$  READ_SKS
;
;      Compiled Functions:
;
; MODIFICATION HISTORY:
; Written:  Samuel LeBlanc, October 14th, 2012, Ottawa, Canada
; Modified: April 25th, 2013, Boulder, Colorado
;           - by Samuel LeBlanc
;           - Modified the reader to take in the new modified SKS files
;           - these files now report end of scan signals for the ingaas as well
; Modified: April 30th, 2013, Ottawa, Ontario, Canada
;           - by Samuel LeBlanc
;           - Modified reader to read in the starting comment of each file
;           - Modified reader to read the 16 bit spectra variables instead of the 32 bit (int instead of lon)
;           - Uses all files made from SKS post April 25th, 2013
; Modified: February, 2014, Boulder, Colorado
;           - by Logan Wright
;           - Modified reader to separate dark and signal spectra
;           - Modified reader to avoid arbitrarily large pre-allocation
; Modified: July 30, 2014, Boulder Colorado
;           - by Shi Song
;           - Modified reader to correctly discard one dark sample on each side of the dark array
; Modified: August 7, 2014, Starbucks on 30th and Arapahoe, Boulder, Colorado
;           - by Shi Song
;           - added outputs of the date(s) to be used for the *_data.pro
;           - separate temperatures into spect_temps & dark_temps
;           - increased the version to read_sks7
; Modified: September 3, 2014, Starbucks on 30th and Arapahoe, Boulder, Colorado
;           - by Shi Song
;           - reversed ingas data array immediately after reading the data
;           - increased the version to read_sks8
; Modified: September 6, 2014, onboard NASA C130 aircraft
;           - by Shi Song
;           - modified the data structure to read the new data after 20140904
;           -  ****FOR DATA COLLECTED ON AND BEFORE 20140904 USE READ_SKS8.PRO****
;           - increased the version to read_sks9
; Modified: October 22, 2014, LASP/CU Boulder
;           - by Shi Song
;           - stored the second set of date/time measurements and used them for calibration date/time
; Modified: October 28, 2014, LASP/CU Boulder
;           - by Shi Song
;           - !!!!!MAJOR MODIFICATION!!!!!
;           - added identificaiton of dark cycles and light cycles within one file
;           - added more output variables to hand over information on the cycles.
;           - increased the version to read_sks10
;---------------------------------------------------------------------------

pro read_SKS, file, count, eos, shutter, inttime, $
              spect, spect_temps, spect_UTC, spect_DATE, spect_ind, specst, speced, $
              darks, dark_temps,  dark_UTC,  dark_DATE,  dark_ind,  darkst, darked

    f=file_search(file,count=m)
    if m ne 1 then message, 'File does not exist or more than one file.'

    ; Preallocate appropriate values
    count   = 0
    ndarks  = 0
    old_shut= 1

    ; 0 - si zenith
    ; 2 - si nadir
    ; 1 - ingaas zenith
    ; 3 - ingaas nadir

    ;set the data structure
    data={frac_sec:double(0),sec:long(0),min:long(0),hour:long(0),day:long(0),month:long(0),year:long(0),dow:long(0),doy:long(0),DST:long(0),$
        frac_sec0:double(0),sec0:long(0),min0:long(0),hour0:long(0),day0:long(0),month0:long(0),year0:long(0),dow0:long(0),doy0:long(0),DST0:long(0),$
        nul:long(0),temp:dblarr(9),$
        int_timesz:long(0),shuttersz:byte(0),EOSsz:byte(0),nul1:long(0),Spectrasz:intarr(257),$
        int_timesn:long(0),shuttersn:byte(0),EOSsn:byte(0),nul2:long(0),Spectrasn:intarr(257),$
        int_timeiz:long(0),shutteriz:byte(0),EOSiz:byte(0),nul3:long(0),Spectraiz:intarr(257),$
        int_timein:long(0),shutterin:byte(0),EOSin:byte(0),nul4:long(0),Spectrain:intarr(257)}

    print,'Open: '+f[0]
    openr,lun,f[0],/get_lun
    on_ioerror,out
    com={nul:byte(0),comment:bytarr(144),nul1:byte(0),nul2:byte(0),nul3:byte(0)}
    readu,lun,com
    comment=''
    for i=0, 143 do comment=comment+string(com.comment[i])
    if com.nul ne 144 then Point_lun,lun,0 ; if the comment does not make sense, then return to start and run the typical reader
    WHILE NOT eof(lun) DO BEGIN

        readu,lun,data ;must verify that each array has some values (check nul and spctsz before reading the temps and the spectra)

        IF data.nul1 NE 257 THEN message, 'data corrupt'

        if(data.day lt 10) then begin
            myday  = strcompress('0' + string(data.day), /remove_all)
        endif else begin
            myday  = strcompress(string(data.day), /remove_all)
        endelse
        if(data.day0 lt 10) then begin
            myday0 = strcompress('0' + string(data.day0), /remove_all)
        endif else begin
            myday0 = strcompress(string(data.day0), /remove_all)
        endelse
        if(data.month lt 10) then begin
            mymon  = strcompress('0' + string(data.month), /remove_all)
        endif else begin
            mymon  = strcompress(string(data.month), /remove_all)
        endelse
        if(data.month0 lt 10) then begin
            mymon0 = strcompress('0' + string(data.month0), /remove_all)
        endif else begin
            mymon0 = strcompress(string(data.month0), /remove_all)
        endelse
        myyear  = strcompress(string(data.year), /remove_all)
        myyear0 = strcompress(string(data.year0), /remove_all)

        ;put into arrays
        IF count EQ 0 THEN BEGIN
            spect0=data.Spectrasz
            eos0=data.EOSsz
            shutter0=data.shuttersz
            inttime0=data.int_timesz

            spect2=data.Spectrasn
            eos2=data.EOSsn
            shutter2=data.shuttersn
            inttime2=data.int_timesn

            spect1=data.Spectraiz
            eos1=data.EOSiz
            shutter1=data.shutteriz
            inttime1=data.int_timeiz

            spect3=data.Spectrain
            eos3=data.EOSin
            shutter3=data.shutterin
            inttime3=data.int_timein

            temps    = data.temp
            UTC      = double(data.hour)+double(data.min)/60.d0+double(data.sec)/3600.d0+double(data.frac_sec)/3600.d0
            UTC0     = double(data.hour0)+double(data.min0)/60.d0+double(data.sec0)/3600.d0+double(data.frac_sec0)/3600.d0
            seconds0 = double(data.sec0)
            DATE     = myyear+mymon+myday
            DATE0    = myyear0+mymon0+myday0
        ENDIF ELSE BEGIN

            spect0=[[spect0],[data.Spectrasz]]
            eos0=[eos0,data.EOSsz]
            shutter0=[shutter0,data.shuttersz]
            inttime0=[inttime0,data.int_timesz]

            spect2=[[spect2],[data.Spectrasn]]
            eos2=[eos2,data.EOSsn]
            shutter2=[shutter2,data.shuttersn]
            inttime2=[inttime2,data.int_timesn]

            spect1=[[spect1],[data.Spectraiz]]
            eos1=[eos1,data.EOSiz]
            shutter1=[shutter1,data.shutteriz]
            inttime1=[inttime1,data.int_timeiz]

            spect3=[[spect3],[data.Spectrain]]
            eos3=[eos3,data.EOSin]
            shutter3=[shutter3,data.shutterin]
            inttime3=[inttime3,data.int_timein]

            temps    = [[temps],[data.temp]]
            UTC      = [UTC,double(data.hour)+double(data.min)/60.d0+double(data.sec)/3600.d0+double(data.frac_sec)/3600.d0]
            UTC0     = [UTC0,double(data.hour0)+double(data.min0)/60.d0+double(data.sec0)/3600.d0+double(data.frac_sec0)/3600.d0]
            seconds0 = [seconds0, double(data.sec0)]
            DATE     = [DATE,myyear+mymon+myday]
            DATE0    = [DATE0,myyear0+mymon0+myday0]
        ENDELSE

        count=count+1
    ENDWHILE
    out:
    free_lun,lun

    ; Reform Data into 3D Array
    spect   = [[[transpose(spect0)]],[[transpose([spect1[0,*],reverse(spect1[1:*,*],1)])]],[[transpose(spect2)]],[[transpose([spect3[0,*],reverse(spect3[1:*,*],1)])]]]
    eos     = [[eos0],[eos1],[eos2],[eos3]]
    inttime = [[inttime0],[inttime1],[inttime2],[inttime3]]
    temps   = transpose(temps)

    ; Check that all shutter signals are the same. Otherwise Error!
    IF mean(abs(shutter0-shutter1)) EQ 0 AND mean(abs(shutter2-shutter3)) EQ 0 AND mean(abs(shutter0-shutter3)) EQ 0 THEN BEGIN
        shutter = shutter0
    ENDIF ELSE BEGIN
        print, 'ERROR: Spectrometer Shutter Times Do Not Match'
    ENDELSE

    ; Check for darks using shutter signal
    ; The shutter signals should be the same for each shutter, so just use shutter0
    ; NOTE:  1 = Closed Shutter
    ;        0 = Open Shutter

    dark_ind0 = where(shutter EQ 1,/NUll,ndarks)
    ndborder=2

    if ndarks gt 2 then begin
        tmp   =  dark_ind0[1:ndarks-1]-dark_ind0[0:ndarks-2]
        edge  =  where(tmp gt 1, ndcycle)
        if ndcycle gt 0 then begin
            darkst0 = [0,edge+1]
            darked0 = [edge, ndarks-1]
        endif else begin
            if ndarks gt ndborder*2+1 then begin
                dark_ind = dark_ind0[ndborder:ndarks-ndborder-1]
                darkst = [long(0)]
                darked = [n_elements(dark_ind)-1]
            endif else begin
                goto, notenoughdarks
            endelse
        endelse

        if ndarks gt ndcycle*ndborder+1 then begin
            ; Discard ndborder number of darks on either side of the shutter signal
            if ndcycle gt 0 then begin
                darkst1 = darkst0+ndborder
                darked1 = darked0-ndborder
                dark_ind = intarr(ndarks-ndcycle*ndborder)
                for nn=0, ndcycle do begin
                    if darkst1[nn] gt darked1[nn] then continue
                    if nn eq 0 then dark_ind = dark_ind0[darkst1[0]:darked1[0]] else $
                        dark_ind = [dark_ind,dark_ind0[darkst1[nn]:darked1[nn]]]
                endfor
                tmp = dark_ind[1:n_elements(dark_ind)-1]-dark_ind[0:n_elements(dark_ind)-2]
                darkst = [0,where(tmp gt 1)+1]
                darked = [where(tmp gt 1), n_elements(dark_ind)-1]
            endif

            ; Pull selected values from raw spectra array to dark array
            darks      = reform(spect[dark_ind,*,*])
            if mean(UTC[dark_ind]) eq 0 and DATE[dark_ind[0]] eq '19040101' then begin
                dark_UTC   = reform(UTC0[dark_ind])
                dark_DATE  = reform(DATE0[dark_ind])
            endif else begin
                dark_UTC   = reform(UTC[dark_ind])
                dark_DATE  = reform(DATE[dark_ind])
            endelse
            dark_temps = reform(temps[dark_ind,*])
            nul = where(dark_UTC eq 0.0, cnt0)
            ;if cnt0 ge 1 then stop
        endif else begin
            goto, notenoughdarks
        endelse

    endif else begin
        notenoughdarks:
        darks      = 0
        dark_UTC   = 0
        dark_DATE  = 0
        dark_temps = 0
        dark_ind   = -1
        darkst     = -1
        darked     = -1
    endelse

    spect_ind0 = where(shutter EQ 0,/NULL,nspects)
    nsborder=1
    if nspects gt 2 then begin
        tmp = spect_ind0[1:nspects-1]-spect_ind0[0:nspects-2]
        edge  =  where(tmp gt 1, nscycle)
        if nscycle gt 0 then begin
            specst0 = [0,edge+1]
            speced0 = [edge, nspects-1]
        endif else begin
            if nspects gt nsborder*2+1 then begin
                spect_ind = spect_ind0[nsborder:nspects-nsborder-1]
                specst = [long(0)]
                speced = [n_elements(spect_ind)-1]
            endif else begin
                goto, notenoughlights
            endelse
        endelse

        if nspects gt nsborder*nscycle then begin
            if nscycle gt 0 then begin
                ; Discard nsborder number of lights on either side of the shutter signal
                specst1 = specst0+nsborder
                speced1 = speced0-nsborder
                spect_ind = intarr(nspects-nscycle*nsborder)
                for nn=0, nscycle do begin
                    if specst1[nn] gt speced1[nn] then continue
                    if nn eq 0 then spect_ind = spect_ind0[specst1[0]:speced1[0]] else $
                        spect_ind = [spect_ind,spect_ind0[specst1[nn]:speced1[nn]]]
                endfor
                tmp = spect_ind[1:n_elements(spect_ind)-1]-spect_ind[0:n_elements(spect_ind)-2]
                specst = [0,where(tmp gt 1)+1]
                speced = [where(tmp gt 1), n_elements(spect_ind)-1]
            endif

            ; Pull selected values from raw spectra array to data array
            spect       = reform(spect[spect_ind,*,*])
            if mean(UTC[spect_ind]) eq 0 and DATE[spect_ind[0]] eq '19040101' then begin
                spect_UTC   = reform(UTC0[spect_ind])
                spect_DATE  = reform(DATE0[spect_ind])
            endif else begin
                spect_UTC   = reform(UTC[spect_ind])
                spect_DATE  = reform(DATE[spect_ind])
            endelse
            spect_temps = reform(temps[spect_ind,*])
        endif else begin
            goto, notenoughlights
        endelse
    endif else begin
        notenoughlights:
        spect       = 0
        spect_UTC   = 0
        spect_DATE  = 0
        spect_temps = 0
        spect_ind   = -1
        specst      = -1
        speced      = -1
    endelse

    ;stop
END
