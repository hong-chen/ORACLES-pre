;+
; NAME:
;   read_SKS
;
; PURPOSE:
;   Program to read in the '*.SKS' binary files produced by SKS spectrometer system
;
; CATEGORY:
;   SKS read data
;
; CALLING SEQUENCE:
;   read_SKS, file, spect, eos, shutter, inttime, temps, spect_UTC, count, ndarks, darks, dark_UTC
;   - file: file to be opened and read
;   - spect: array of all spectra (including the darks) for all spectrometers
;            countx256x4
;            0 - Si Zenith, 2 - In Zenith, 1 - Si Nadir, 3 - In Nadir
;   - eos: end of signal check for each time and spectrometer
;   - shutter: shutter signal (opened=1, closed=0) for each spectrometer
;   - inttime: integration time for each spectrometer and time
;   - temps: array of temperatures
;   - UTC: time in UTC format (hours)
;   - count: number of spectra in file (including darks
;   - ndarks: number of darks in file
;   - darks: array of dark spectra
;
; OUTPUT:
;   arrays of measurements
;
; KEYWORDS:
;   none
;
; DEPENDENCIES:
;   none
;
; NEEDED FILES:
;   - *.SKS files
;
; EXAMPLE:
; IDL> file='../data/spc00001.SKS'
; IDL> .r read_SKS
; % Compiled module: READ_SKS.
; IDL> read_SKS,file, spect, eos, shutter, inttime, temps, UTC, count,ndarks,darks,comment
; IDL> help
; % At $MAIN$
; COMMENT         STRING    = 'File autostart - settings:
;                              si_rad:100 ms
;                              si_irr:100 ms
;                              In_rad:300 ms
;                              InGas splitter removed for signal
;                              In_irr:300 ms                         '
; COUNT           INT       =      199
; DARKS           FLOAT     = Array[9, 256, 4]
; EOS             INT       = Array[200, 4]
; FILE            STRING    = '../data/spc00001.SKS'
; INTTIME         INT       = Array[200, 4]
; NDARKS          INT       =        8
; SHUTTER         INT       = Array[200, 4]
; SPECT           FLOAT     = Array[200, 256, 4]
; TEMPS           DOUBLE    = Array[200, 9]
; UTC             DOUBLE    = Array[200]
; Compiled Procedures:
;     $MAIN$  READ_SKS
;
; Compiled Functions:
;
;
; MODIFICATION HISTORY:
; Written:  Samuel LeBlanc, October 14th, 2012, Ottawa, Canada
; Modified: April 25th, 2013, Boulder, Colorado
;           - by Samuel LeBlanc
;           - Modified the reader to take in the new modified SKS files
;           - these files now report end of scan signals for the ingaas as well
; Modified: April 30th, 2013, Ottawa, Ontario, Canada
;           - by Samuel LeBlanc
;           - Modified reader to read in the starting comment of each file
;           - Modified reader to read the 16 bit spectra variables instead of the 32 bit (int instead of lon)
;           - Uses all files made from SKS post April 25th, 2013
; Modified: February, 2014, Boulder, Colorado
;           - by Logan Wright
;           - Modified reader to separate dark and signal spectra
;           - Modified reader to avoid arbitrarily large pre-allocation
; Modified: July 30, 2014, Boulder Colorado
;           - by Shi Song
;           - Modified reader to correctly discard one dark sample on each side of the dark array
;           - increased the version to read_sks6
;---------------------------------------------------------------------------

pro read_SKS, file, spect, eos, shutter, inttime, temps, $
              spect_UTC, count, ndarks, darks, dark_UTC

    f=file_search(file,count=m)
    if m ne 1 then message, 'File does not exist or more than one file.'

    ; Preallocate appropriate values
    count   = 0
    ndarks  = 0
    old_shut= 1

    ; 0 - si zenith
    ; 2 - si nadir
    ; 1 - ingaas zenith
    ; 3 - ingaas nadir (Not installed on SSIR as of 2/17/2014)

    ;set the data structure
    data={frac_sec:double(0),sec:long(0),min:long(0),hour:long(0),day:long(0),month:long(0),year:long(0),dow:long(0),doy:long(0),DST:long(0),$
        nul:long(0),temp:dblarr(9),$
        int_timesz:long(0),shuttersz:byte(0),EOSsz:byte(0),nul1:long(0),Spectrasz:intarr(257),$
        int_timesn:long(0),shuttersn:byte(0),EOSsn:byte(0),nul2:long(0),Spectrasn:intarr(257),$
        int_timeiz:long(0),shutteriz:byte(0),EOSiz:byte(0),nul3:long(0),Spectraiz:intarr(257),$
        int_timein:long(0),shutterin:byte(0),EOSin:byte(0),nul4:long(0),Spectrain:intarr(257)}

    print,'Open: '+f[0]
    openr,lun,f[0],/get_lun
    on_ioerror,out
    com={nul:byte(0),comment:bytarr(144),nul1:byte(0),nul2:byte(0),nul3:byte(0)}
    readu,lun,com
    comment=''
    for i=0, 143 do comment=comment+string(com.comment[i])
    if com.nul ne 144 then Point_lun,lun,0 ; if the comment does not make sense, then return to start and run the typical reader
    WHILE NOT eof(lun) DO BEGIN

        readu,lun,data ;must verify that each array has some values (check nul and spctsz before reading the temps and the spectra)

        IF data.nul1 NE 257 THEN message, 'data corrupt'

        ;put into arrays
        IF count EQ 0 THEN BEGIN

            spect0=data.Spectrasz
            eos0=data.EOSsz
            shutter0=data.shuttersz
            inttime0=data.int_timesz

            spect2=data.Spectrasn
            eos2=data.EOSsn
            shutter2=data.shuttersn
            inttime2=data.int_timesn

            spect1=data.Spectraiz
            eos1=data.EOSiz
            shutter1=data.shutteriz
            inttime1=data.int_timeiz

            spect3=data.Spectrain
            eos3=data.EOSin
            shutter3=data.shutterin
            inttime3=data.int_timein

            temps=data.temp
            UTC=double(data.hour)+double(data.min)/60.d0+double(data.sec)/3600.d0+double(data.frac_sec)/3600.d0
        ENDIF ELSE BEGIN

            spect0=[[spect0],[data.Spectrasz]]
            eos0=[eos0,data.EOSsz]
            shutter0=[shutter0,data.shuttersz]
            inttime0=[inttime0,data.int_timesz]

            spect2=[[spect2],[data.Spectrasn]]
            eos2=[eos2,data.EOSsn]
            shutter2=[shutter2,data.shuttersn]
            inttime2=[inttime2,data.int_timesn]

            spect1=[[spect1],[data.Spectraiz]]
            eos1=[eos1,data.EOSiz]
            shutter1=[shutter1,data.shutteriz]
            inttime1=[inttime1,data.int_timeiz]

            spect3=[[spect3],[data.Spectrain]]
            eos3=[eos3,data.EOSin]
            shutter3=[shutter3,data.shutterin]
            inttime3=[inttime3,data.int_timein]

            temps  =[[temps],[data.temp]]
            UTC = [UTC,double(data.hour)+double(data.min)/60.d0+double(data.sec)/3600.d0+double(data.frac_sec)/3600.d0]
        ENDELSE

        count=count+1
    ENDWHILE
    out:
    free_lun,lun

    ; Reform Data into 3D Array
    spect = [[[transpose(spect0)]],[[transpose(spect1)]],[[transpose(spect2)]],[[transpose(spect3)]]]

    ; XXX First dimension is time
    ; But why now 257, not 256??????? XXX

    eos = [[eos0],[eos1],[eos2],[eos3]]
    inttime = [[inttime0],[inttime1],[inttime2],[inttime3]]
    temps = transpose(temps)

    ; Check that all shutter signals are the same. Otherwise Error!
    IF mean(abs(shutter0-shutter1)) EQ 0 AND mean(abs(shutter2-shutter3)) EQ 0 AND mean(abs(shutter0-shutter3)) EQ 0 THEN BEGIN
        shutter = shutter0
    ENDIF ELSE BEGIN
        print, 'ERROR: Spectrometer Shutter Times Do Not Match'
    ENDELSE

    ; Check for darks using shutter signal
    ; The shutter signals should be the same for each shutter, so just use shutter0
    ; NOTE:  1 = Closed Shutter
    ;        0 = Open Shutter

    dark_ind = where(shutter EQ 1,/NUll,ndarks)
    if ndarks ge 1 then begin
        ; Discard nborder number of values on either side of the shutter signal
        ; Experiments suggests two spectra on either side of the shutter border should be discarded.
        nborder=1
        dark_ind = dark_ind[nborder:n_elements(dark_ind)-1-nborder]
        ; Pull selected values from raw spectra array to dark array
        darks = spect[dark_ind,*,*]
        dark_UTC = UTC[dark_ind]

    endif else begin
        darks=0
    endelse

    spec_ind = where(shutter EQ 0,/NULL)
    spect = spect[spec_ind,*,*]
    spect_UTC = UTC[spec_ind]

    ;XXX I have no idea what the following means/does, commented out.
    ; Check to make sure shutter signals are correct by thresholding the spectra
    ; Note: We need a better solution here
    ;threshold = 1000
    ;darkmax = max(darks[*,*,0], DIMENSION=2)
    ;darks = darks[where(darkmax LT threshold),*,*]
    ;dark_UTC = dark_UTC[where(darkmax LT threshold),*,*]
    ;spectmax = max(spect[*,*,0], DIMENSION=2)
    ;spect = spect[where(spectmax GT threshold),*,*]
    ;spect_UTC = spect_UTC[where(spectmax GT threshold),*,*]

END
