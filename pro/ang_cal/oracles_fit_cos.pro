pro fit_cos,mu,sp,lo,hi,mu_new,sp_new,max_error=max_error,verbose=verbose

  ; subroutine makes fit of sp(mu) on new mu grid with 0.001 resolution
  ; fit line to part with mu<lo
  xxx=float(indgen(fix(lo*1000.+0.5)))*0.001
  ind=where(mu le lo and mu gt 1e-10 and sp gt 1e-10)
  coeff=poly_fit(mu[ind],sp[ind],1)
  ;coeff=linfit(mu[ind],sp[ind])
  line_old=coeff[0]+coeff[1]*xxx
  ;oplot,xxx,line_old

  ; fit polynomial to part where mu>hi
  xxxx=hi+float(indgen(fix(1000.-1000*hi+0.5)))*0.001+0.001
  ind2=where(mu lt hi) ; Use all the mu's above hi, and the first below
  mu_min=max(mu[ind2])
  ind=where(mu ge mu_min)
  coef=poly_fit(mu[ind],sp[ind],4,status=status)
  if status gt 0 and keyword_set(verbose) then print,'Problem with POLYFIT.'
  ;normalize such that the function becomes 1 at 1
  normo=coef[0]+coef[1]*1.0+coef[2]*1.0^2 +coef[3]*1.0^3+coef[4]*1.0^4
  coef =coef/normo
  curve_old= coef[0]+coef[1]*xxxx+coef[2]*xxxx^2 +coef[3]*xxxx^3+coef[4]*xxxx^4
  ;oplot,xxxx,curve_old

  ; link the two parts
  xx=lo+indgen(fix((hi-lo)*1000.+1.5))*0.001
  f_upo = coef[0]+coef[1]*hi + coef[2]*hi^2 + coef[3]*hi^3+coef[4]*hi^4
  f_loo = coeff[0] + lo*coeff[1]
  der_upo = coef[1] + 2.0*hi*coef[2] + 3.0*hi^2*coef[3] + 4.0*hi^3*coef[4]
  der_loo = coeff[1]
  ; coefficients for linking curve
  dol =  (2.0*(f_upo- f_loo) + (lo-hi)*(der_loo+der_upo))/ ((lo-hi)^3)
  co = (der_loo - der_upo - 3.0*(lo^2 - hi^2)*dol)/(2.0*(lo-hi))
  bo = der_loo - 2.0*co*lo - 3.0*dol*lo^2
  eo = f_loo - lo*bo - co*lo^2 - dol*lo^3
  link_func_old = eo + bo*xx + co*xx^2 + dol*xx^3
  ;oplot, xx, link_func_old

  mu_new=[xxx, xx, xxxx]
  ;print,max(xxx),lo,min(xx)
  ;print,max(xx),hi,min(xxxx)
  if n_elements(mu_new) ne 1001 then message,'Wrong final resolution.'
  sp_new=[line_old,link_func_old,curve_old]
  if n_elements(sp_new) ne 1001 then message,'Wrong final resolution.'

  if keyword_set(max_error) then begin
    ; checking how far the original data is off from the fitted data [%]
    nm=n_elements(mu)
    dv=fltarr(nm)
    for i=0,nm-1 do begin
      mm=min(abs(mu[i]-mu_new),m0)
      if sp[i] gt 1e-10 then dv[i]=abs(sp[i]-sp_new[m0])/sp[i]*100.
    endfor
    ind=where(dv gt max_error,n)
    if n gt 0 then print,'Cosine fitting exceeded '+string(max_error)+'% for mu=',mu[ind]
  endif

end
