; for angular calibration only

pro read_sks, fname, spec_raw, shsw_raw

  np     = 256
  bignum = 60000L

  spec_raw = fltarr(np, 4, bignum)
  shsw_raw = fltarr(bignum)

  ; 0 - si zenith
  ; 2 - si nadir
  ; 1 - ingaas zenith
  ; 3 - ingaas nadir

  header={nul:byte(0),comment:bytarr(144),nul1:byte(0),nul2:byte(0),nul3:byte(0)}

  data={frac_sec :double(0),sec :long(0),min :long(0),hour :long(0),day :long(0),month :long(0),year :long(0),dow :long(0),doy :long(0),DST :long(0),$
        frac_sec0:double(0),sec0:long(0),min0:long(0),hour0:long(0),day0:long(0),month0:long(0),year0:long(0),dow0:long(0),doy0:long(0),DST0:long(0),$
        nul:long(0),temp:dblarr(9),$
        int_timesz:long(0),shuttersz:byte(0),EOSsz:byte(0),nul1:long(0),Spectrasz:intarr(257),$
        int_timesn:long(0),shuttersn:byte(0),EOSsn:byte(0),nul2:long(0),Spectrasn:intarr(257),$
        int_timeiz:long(0),shutteriz:byte(0),EOSiz:byte(0),nul3:long(0),Spectraiz:intarr(257),$
        int_timein:long(0),shutterin:byte(0),EOSin:byte(0),nul4:long(0),Spectrain:intarr(257)}

  openr, lun, fname, /get_lun

  readu, lun, header
  if header.nul ne 144 then point_lun,lun,0

  count  = 0
  while not eof(lun) do begin

    readu, lun, data
    spec_raw[*, 0, count] = data.Spectrasz[1:*]
    spec_raw[*, 1, count] = data.Spectraiz[1:*]
    spec_raw[*, 2, count] = data.Spectrasn[1:*]
    spec_raw[*, 3, count] = data.Spectrain[1:*]

    shsw_raw[count]       = data.shuttersz

    count=count+1

  endwhile
  free_lun,lun

  spec_raw = spec_raw[*, *, 0:count-1]
  shsw_raw = shsw_raw[0:count-1]

end
