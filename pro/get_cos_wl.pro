; Comment from Sebastian: The correction factor should be around 0.96, and not be wavelength dependent. 
;  For example, line 33 should NOT be "dz[i]=wlz[i]/int_tabulated(dcl,ccl)". The same is true for line 67.
;
;program for getting the cosine correction factor for the entire
;spectrum wavelengths

;Modified August 2016 by Sabrina Cochrane for use with ORACLES
;make_cos.pro output

;@cfg.pro
pro get_cos,file1,file2,sza,dc,wlz,  facz,dz,wln,facn,dn,faczk,facnk

  ; (2) Get cosine response zenith
  cosfile=file_search(file1,count=nc)
  if nc lt 1 then message,'No cosine response file found.'
  if nc gt 1 then message,'More than one cosine response file found.'

  cc=indgen(1001)
  print,'Open: ',cosfile[0]
  restore,cosfile
  ;stop
  dcl=mu*1000
  na=n_elements(mu)
  ;nl=n_elements(wvl)

  facz=dblarr(n_elements(sza),n_elements(wlz))
  faczk=dblarr(n_elements(sza),n_elements(wlz))
  dz=dblarr(n_elements(wlz))

  mu0=cos(sza*!pi/180.)
  cf=fix(dc*1000+0.5); rounding to closest integer
  i0=where(cf gt 1000,in) & if in ge 1 then cf[i0]=1000
  i0=where(cf lt 0,in)   & if in ge 1 then cf[i0]=0
  ;for i=0L, n_elements(wlz)-1L do begin
  ; mm=min(abs(wvl-wlz[i]),in)
  ;ccl=res_wvl[*,in]

  for i=0, n_elements(wlz)-1 do begin
    ;ccl=sp_grd           ; is this right?
    m = min(abs(wlf-wlz[i]),index_wvl)
    ccl = mu/dr[index_wvl, *]
    fcz=interpol(ccl,dcl,cc); ccl is response function 0 to 1 if ideal cosine response,  dcl goes from 0 to 1000, cc is regridding
    dz[i]=500./int_tabulated(dcl,ccl) ; diffuse correction factor for zenith
    faczk[*,i]=fcz[cf]
    ind=where(faczk[*,i] lt 0.01,n0)
    if n0 gt 0 then begin
      faczk[ind,i]=1.
    endif
    facz[*,i]=mu0[*]/faczk[*,i] ; if cosine response 1:1, then fac=1 everywhere
  endfor

  ; (3) Get cosine response nadir
  ;cfp=cfg(cfg,'cosnad')
  cosfile=file_search(file2,count=nc)
  if nc lt 1 then message,'No NAD cosine response file found.'
  if nc gt 1 then message,'More than one cosine response file found.'

  cc=indgen(1001)
  print,'Open: ',cosfile[0]
  restore,cosfile
  dcl=mu*1000

  facn=dblarr(n_elements(sza),n_elements(wln))
  facnk=dblarr(n_elements(sza),n_elements(wln))
  dn=dblarr(n_elements(wln))

  ;for i=0L, n_elements(wln)-1L do begin
  ;  mm=min(abs(wvl-wln[i]),in)
  ;  ccl=res_wvl[*,in]
  for i=0, n_elements(wln)-1 do begin
    ;ccl=sp_grd
    m = min(abs(wlf-wln[i]),index_wvl)
    ccl = mu/dr[index_wvl, *]
    fcn=interpol(ccl,dcl,cc);; need to change/get rid of this? ******************LOOK INTO THIS*************
    dn[i]=500./int_tabulated(dcl,ccl) ; diffuse correction factor

    ; (4) Get correction factors

    facnk[*,i]=fcn[cf]

    ind=where(facnk[*,i] lt 0.01,n0)
    if n0 gt 0 then begin
      facnk[ind,i]=1.
    endif
    facn[*,i]=reform(mu0[*]/facnk[*,i])
  endfor

  return
end
