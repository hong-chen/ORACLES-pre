;+
; NAME:
;    read_hsk_er2
;
; PURPOSE:
;    Read ER2 house keeping data for ORACLES 2016
;
; INPUTS:
; OUTPUTS:
;
; COMMENTS:
;
; MODIFICATION HISTORY:
;     Written by Sebastian Schmidt, 8/16/2013
;     Modified by Sabrina Cochrane 10/25/2016 for ORACLES
;     Modified by Hong Chen 06/01/2017 for ORACLES ER2
;-


pro read_hsk_er2,file,date,utc,lat,lon,alt,palt,gsp,asp,vsp,pit,rol,hed,sza,tst,pst,wsp,wdi,asn,saz,aza

  n=file_lines(file)
  openr,ua,file,/get_lun & str=''
  readf,ua,skip ; read number of header lines to skip
  for i=0,skip-2 do readf,ua,str

  n   = n-skip                    ; # data lines
  if date eq '20160920' then n=n-1
  utc = fltarr(n)
  lat = fltarr(n)
  lon = fltarr(n)
  alt = fltarr(n) ; GPS altitude
  palt= fltarr(n) ; pressure alt
  gsp = fltarr(n) ; ground speed
  asp = fltarr(n) ; true air speed
  mch = fltarr(n) ; mach number
  vsp = fltarr(n) ; vertical speed
  hed = fltarr(n) ; true heading
  tan = fltarr(n) ; track angle
  dft = fltarr(n) ; drift angle
  pit = fltarr(n) ; pitch
  rol = fltarr(n) ; roll angle
  tst = fltarr(n) ; static air temperature
  pot = fltarr(n) ; potential temp
  ttp = fltarr(n) ; total air temp
  pst = fltarr(n) ; static pressure
  cps = fltarr(n) ; cabin pressure
  wsp = fltarr(n) ; wind speed
  wdi = fltarr(n) ; wind direction
  sza = fltarr(n) ; solar zenith angle
  asn = fltarr(n) ; aircraft sun elevation
  saz = fltarr(n) ; sun azimuth
  aza = fltarr(n) ; aircraft sun azimuth

  for i=0,n-1 do begin
    readf,ua,str
    data=float(strsplit(str,',',/extract,/preserve_null))
    utc[i] = data[0]
    lat[i] = data[1]
    lon[i] = data[2]
    alt[i] = data[3]
    palt[i]= data[4]
    gsp[i] = data[5]
    asp[i] = data[6]
    mch[i] = data[7]
    vsp[i] = data[8]
    hed[i] = data[9]
    tan[i] = data[10]
    dft[i] = data[11]
    pit[i] = data[12]
    rol[i] = data[13]
    tst[i] = data[14]
    pot[i] = data[15]
    ttp[i] = data[16]
    pst[i] = data[17]
    cps[i] = data[18]
    wsp[i] = data[19]
    wdi[i] = data[20]
    sza[i] = data[21]
    asn[i] = data[22]
    saz[i] = data[23]
    aza[i] = data[24]
  endfor

  free_lun,ua
  utc=utc/3600.
  alt=alt*0.001

end
