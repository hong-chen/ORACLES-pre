;+
; NAME:
;    read_hsk_p3
;
; PURPOSE:
;    Read P3 house keeping data for ORACLES 2016
;
; INPUTS:
; OUTPUTS:
;
; COMMENTS:
;
; MODIFICATION HISTORY:
;     Written by Sebastian Schmidt, 8/16/2013
;     Modified by Sabrina Cochrane 10/25/2016 for ORACLES
;-


pro read_hsk_p3, file, date, utc, lat, lon, alt, palt, gsp, asp, isp, vsp, pit, rol, hed, sza, tst, pst, mr, pv, rh, wsp, wdi, asn, saz, aza

  n=file_lines(file)
  openr,ua,file,/get_lun & str=''
  readf,ua,skip ; read number of header lines to skip
  for i=0,skip-2 do readf,ua,str

  n   = n-skip                    ; # data lines
  if date eq 20160920 then n=n-1
  utc = fltarr(n)
  doy = fltarr(n)
  lat = fltarr(n)
  lon = fltarr(n)
  alt = fltarr(n); GPS altitude
  palt= fltarr(n); pressure alt
  ralt= fltarr(n) ; radar alt
  gsp = fltarr(n) ; ground speed
  asp = fltarr(n) ; true air speed
  isp = fltarr(n) ;indicated air speed
  mch = fltarr(n) ;mach number
  vsp = fltarr(n); vertical speed
  hed = fltarr(n)
  tan = fltarr(n); track angle
  dft = fltarr(n); drift angle
  pit = fltarr(n); pitch
  rol = fltarr(n); roll angle
  tst = fltarr(n); static air temperature
  pot = fltarr(n) ;potential temp
  tdp = fltarr(n); dewpoint temp
  ttp = fltarr(n) ;total air temp
  pst = fltarr(n) ; static pressure
  cps = fltarr(n) ; cabin pressure
  wsp = fltarr(n); wind speed
  wdi = fltarr(n) ; wind direction
  sza = fltarr(n); solar zenith angle
  asn = fltarr(n); aircraft sun elevation
  saz = fltarr(n); sun azimuth
  aza = fltarr(n); aircraft sun azimuth
  mr  = fltarr(n); mixing ratio
  pv  = fltarr(n) ; partial pressure water
  sat = fltarr(n) ; sat vapor pressure water
  sti = fltarr(n); sat vapor pressure ice
  rh  = fltarr(n) ; relative humidity

  for i=0,n-1 do begin
    readf,ua,str
    data=float(strsplit(str,',',/extract,/preserve_null))
    utc[i] = data[0]
    doy[i] = data[1]
    lat[i] = data[2]
    lon[i] = data[3]
    alt[i] = data[4]
    palt[i]= data[5]
    ralt[i]= data[6]
    gsp[i] = data[7]
    asp[i] = data[8]
    isp[i] = data[9]
    mch[i] = data[10]
    vsp[i] = data[11]
    hed[i] = data[12]
    tan[i] = data[13]
    dft[i] = data[14]
    pit[i] = data[15]
    rol[i] = data[16]
    tst[i] = data[17]
    pot[i] = data[18]
    tdp[i] = data[19]
    ttp[i] = data[20]
    pst[i] = data[21]
    cps[i] = data[22]
    wsp[i] = data[23]
    wdi[i] = data[24]
    sza[i] = data[25]
    asn[i] = data[26]
    saz[i] = data[27]
    aza[i] = data[28]
    mr[i]  = data[29]
    pv[i]  = data[30]
    sat[i] = data[31]
    sti[i] = data[32]
    rh[i]  = data[33]
  endfor

  free_lun,ua
  utc=utc/3600.
  alt=alt*0.001

end
