;+
; NAME:
; read_plt3
;
;
; PURPROSE:
; A program that reads a series of platform files (*plt2) for the NEON leveling platform and outputs an IDL save file
; with the variables, their names, and UTC times.
;
; CATEGORY:
; Misc.
;
; CALLING SEQUENCE:
; read_platform
; Make sure to run the procedure inside the directory that contains the platform files ".plt2" 
;
; INPUT:
; *.plt2 files for example: 20150617_pos00001.plt2,20150617_pos00002.plt2,20150617_pos00003.plt2...
;
; OUTPUT:
; The ouput from files shown above would be date_platform.sav. Simply restore this file from the IDL command line.
;
; KEYWORDS:
; None
;
; DEPENDENCIES:
; gps2utc.pro
; caldates.pro
;
; NEEDED FILES:
; *plt2 files are required
;
; MODIFICATION HISTORY
;
; Written: Bruce Kindel, November 10, 2015, Boulder, Colorado
; Modified Sabrina Cochrane, June 14, 2014 CU to include variables for
; ORACLES system

; dpita=3.8

@sub_gps2utc.pro
@sub_caldates.pro
pro read_plt3,path,utc,pits,pita,pitm,piti,rols,rola,rolm,roli,alt,lati,loni,smooth=smooth,dpa=dpa,dra=dra

  ; platform format (29 variables by number of records, nominally 12000 for a file, may bet less (e.g. first and/or last)
  ; 0  - Computer hours
  ; 1  - Computer minutes
  ; 2  - Computer seconds
  ; 3  - GPS Time
  ; 4  - GPS Week
  ; 5  - Velocity North (Span CPT)
  ; 6  - Velocity East (Span CPT)
  ; 7  - Velocity Up (Span CPT)
  ; 8  - Pitch (Span CPT)
  ; 9  - Roll (Span CPT)
  ; 10 - Latitude (Span CPT)
  ; 11 - Longitude (Span CPT)
  ; 12 - Height (Span CPT)
  ; 13 - Span CPT Status
  ; 14 - Motor Pitch (deg)
  ; 15 - Motor Roll (deg)
  ; 16 - Inclinometer temp
  ; 17 - Motor Roll Temp
  ; 18 - Motor Pitch Temp
  ; 19 - Stage Temp
  ; 20 - Rel Humidity
  ; 21 - Chassis Temp
  ; 22 - System Voltage
  ; 23 - Roll (ARINC)
  ; 24 - Pitch (ARINC)
  ; 25 - Inclinometer roll (raw voltage)
  ; 26 - Inclinometer Pitch (raw voltage)
  ; 27 - Inclinometer roll (deg)
  ; 28 - Inclinometer pitch (deg)


  if n_elements(path) ne 1 then path= '/Users/sabrinacochrane/data/oracles/p3/20160805/'
  files=file_search(path+'*.plt3',count=count)


  if n_elements(dpa) ne 1 then dpa=3.8
  if n_elements(dra) ne 1 then dra=1.1

  alldata=dblarr(29,(count*12000.))
  counter=0

  for i=0,count-1 do begin
    fileinfo=file_info(files[i])
    numrecords=fileinfo.size/(29*8); spc changed 29/8 to 29/8 to get integer value
    ;print,files[i],'Number of records=',numrecords
    get_lun,u
    temp=dblarr(29,numrecords)
    openr,u,files[i]
    readu,u,temp
    free_lun,u
    alldata[*,counter:(counter+numrecords-1)]=temp
    counter=counter+numrecords
    ;print,'Counter=',counter
  endfor

  maxtime=max(alldata[3,*],indexmax)
  alldata=alldata[*,0:indexmax]

  goodtimes=where(alldata[3,*] gt 1)
  alldata=alldata[*,goodtimes]

  gps2utc,alldata[3,*],0,year,doy,utc,month,day,hour,min,sec
  gpsutc=reform(hour+(min/60.)+(sec/3600.))

  alldatavalues1=['Computer hours,','Computer minutes,','Computer seconds,','GPS Time,','GPS Week,','Velocity N,','Velocity E,','Velocity Up,','Pitch,','Roll,','Lat,','Lon,','Height,','SPAN CPT status,','Motor Pitch,','Motor Roll,','Inclinometer Temp,']
  alldatavalues2=['Motor Roll Temp,','Motor Pitch Temp,','Motor Stage Temp,','Rel Humidity,','Chassis Temp,','System Voltage,']
  alldatavalues3=['Roll ARINC,', 'Pitch ARINC,','Inclinometer roll (raw voltage),','Inclinometer pitch (raw voltage),','Inclinometer Roll (deg),','Inclinometer Pitch (deg),']
  alldatanames=[alldatavalues1,alldatavalues2, alldatavalues3]

  utc=alldata[3,*]/86400.
  day=fix(utc[0])
  utc=utc-day
  utc=utc*24.
  ;u0=utc[0]
  ;while u0 gt 24 do begin
  ;   utc[where(utc gt 24)]=utc[where(utc gt 24)]-24
  ;   ;  ;u0=min(utc)
  ;   u0=utc[0]
  ;endwhile

  pits=alldata[8,*]
  rols=alldata[9,*]
  pitm=alldata[14,*]
  rolm=alldata[15,*]
  pita=alldata[24,*]-dpa
  rola=alldata[23,*]-dra
  piti=alldata[28,*]
  roli=alldata[27,*]
  alt =alldata[12,*]
  lati=alldata[10,*]
  loni=alldata[11,*]

  if n_elements(smooth) gt 0 then begin
    piti=smooth(piti,smooth)
    roli=smooth(roli,smooth)
  endif

  ;savefilename=path+'platform.sav'
  ;save,alldata,gpsutc,alldatanames,file=savefilename
  ;print,'Wrote out IDL save file with data:',savefilename

end
