; CALDATES
;
; PURPOSE:
; Return the calendar date and time given julian date.
; This is the inverse of the function JULDAY.
; CATEGORY:
; Misc.
;
; CALLING SEQUENCE:
; CALDATES, Julian, Month, Day, Year, Hour, Minute, Second
; See also: julday, the inverse of this function.
;
; INPUTS:
; JULIAN contains the Julian Day Number (which begins at noon) of the
; specified calendar date.  It should be a long integer.
; OUTPUTS:
; (Trailing parameters may be omitted if not required.)
; MONTH:  Number of the desired month (1 = January, ..., 12 = December).
;
; DAY:  Number of day of the month.
;
; YEAR: Number of the desired year.
;
; HOUR: Hour of the day
; Minute: Minute of the day
; Second: Second (and fractions) of the day.
;
; COMMON BLOCKS:
; None.
;
; SIDE EFFECTS:
; None.
;
; RESTRICTIONS:
; Accuracy using IEEE double precision numbers is approximately
; 1/10000th of a second.
;
; MODIFICATION HISTORY:
; Translated from "Numerical Recipies in C", by William H. Press,
; Brian P. Flannery, Saul A. Teukolsky, and William T. Vetterling.
; Cambridge University Press, 1988 (second printing).
;
; DMS, July 1992. (CALDAT)
; DMS, April 1996, Added HOUR, MINUTE and SECOND keyword
; AB, 7 December 1997, Generalized to handle array input.
;       SCS, December 2001, True array version (CALDATES)
;-
;

pro CALDATES, Julian, Month, Day, Year, Hour, Minute, Second

  ON_ERROR, 2 ; Return to caller if errors

  IGREG = 2299161L ;Beginning of Gregorian calendar

  jul = long(julian + .5d) ;Better be long
  f = julian + .5d - jul
  hour = floor(f * 24.)
  f = f - hour / 24.d
  minute = floor(f*1440)
  second = (f - minute/1440.d0) * 86400.0d0

  jalpha = long(((jul - 1867216) - 0.25d0) / 36524.25)
  ja = jul + 1 + jalpha - long(0.25d0 * jalpha)
  w = where(jul lt igreg)
  if w[0] ne -1 then ja[w]=jul[w]

  jb = ja + 1524
  jc = long(6680.0 + ((jb-2439870)-122.1d0)/365.25)
  jd = long(365 * jc + (0.25 * jc))
  je = long((jb - jd) / 30.6001)

  day = jb - jd - long(30.6001 * je)
  month = je -1
  w = where(month gt 12)
  if w[0] ne -1 then month[w] = month[w] - 12
  year = jc - 4715
  w = where(month gt 2)
  if w[0] ne -1 then year[w] = year[w] - 1
  w = where(year le 0)
  if w[0] ne -1 then year[w] = year[w] - 1

end
