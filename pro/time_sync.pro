pro time_sync,utc, utc_ssfr, utc_plt, pits, pitm, pita, piti, pit, zspectra, asn
  ;THIS PROGRAM NOT DONE OR WORKING YET
  if 1 then begin
    start=1
    offset=0
    ds=0
    xr1=11.87
    xr2=12.23
    while start do begin
      ;offset=1
      window,0
      !p.multi=0
      plot,utc,cos(!pi/180.*(90-asn)),xr=[xr1,xr2], color=30, yrange=[0.3,1.2]
      dat=zspectra[100,*]
      mx =max(dat)
      oplot,utc_ssfr+ds,dat/mx,color=120
      legend, ['utc', 'utc_ssfr'], textcolor=[30,120]

      print,'Click on SSFR data'
      cursor,xs,ys
      wait,0.2
      print,'Click on NAV data'
      cursor,xn,yn
      wait, 0.2
      print,'Offset: ',xs-xn
      ds=ds-(xs-xn)
      offset=xs-xn
      xr1=xn-2*offset
      xr2=xn+2*offset
      if abs(offset) gt .0001 then start=1 else start=0
    endwhile
      print, 'Add ds=' , ds, ' to Configuration file'
      stop
  endif

  ; --> Add synchronization for ALP data here, you can use something
  ; like this:
  start=1
  offsetp=0
  dp=0
  xr1=11.87
  xr2=12.23
  while start do begin
    window, 1
    !p.multi=0
    plot,utc_plt+dp,pits,xr=[xr1,xr2],yr=[-10,10], color=30
    oplot,utc_plt+dp,-1*pitm,color=120
    oplot, utc_plt+dp, pita, color=60
    oplot, utc_plt+dp, piti, color=90
    oplot, utc, pit, color=150
    ;oplot,utc_plt+offsetm,(zen[0,*]-1.5)*10,color=50,thick=2
    legend, ['ALP S', 'ALP -1*Motor', 'ALP A','ALP Inclinometer', 'Nav' ], textcolor=[30,120, 60,90, 150]
    print, 'Click on ALP (green/blue) data'
    cursor, xa,ya
    wait, 0.2
    print, 'Click on Nav data'
    cursor, xnav, ynav
    wait, 0.2
    offsetp=xa-xnav
    print, 'Offset ',offsetp
    dp=dp-offsetp; -offset if plt is ahead
    print, 'DP= ', dp
    xr1=xnav-4*offsetp
    xr2=xnav+4*offsetp

    if abs(offsetp) gt .0001 then start=1 else start=0
  endwhile
  print, 'Add dp= ',dp,' to CFG file'
  stop
end
