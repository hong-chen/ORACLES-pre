import os
import sys
import glob
import datetime
import multiprocessing as mp
import h5py
import numpy as np
from scipy import interpolate
from scipy.io import readsav

def READ_HSK_ER2(fname):

    data = np.genfromtxt(fname, skip_header=60, delimiter=',')
    tmhr = data[:, 0]/3600.0
    lat  = data[:, 1]
    lon  = data[:, 2]
    alt  = data[:, 3]/1000.0
    sza  = data[:, -4]

    return tmhr, lon, lat, alt, sza

def READ_HSK_P3(fname):

    data = np.genfromtxt(fname, skip_header=70, delimiter=',')
    tmhr = data[:, 0]/3600.0
    lat  = data[:, 2]
    lon  = data[:, 3]
    alt  = data[:, 4]/1000.0
    sza  = data[:, -9]

    return tmhr, lon, lat, alt, sza

if __name__ == "__main__":
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    from matplotlib.ticker import FixedLocator
    from mpl_toolkits.basemap import Basemap

    fname = '/Users/hoch4240/Chen/work/04_ORACLES/pre/er2/20160920/Housekeeping_ER2_20160920_R0.ict'
    tmhr_er2, lon_er2, lat_er2, alt_er2, sza_er2 = READ_HSK_ER2(fname)

    fname = '/Users/hoch4240/Chen/work/04_ORACLES/pre/p3/20160920/Hskping_P3_20160920_R0.ict'
    tmhr_p3, lon_p3, lat_p3, alt_p3, sza_p3 = READ_HSK_P3(fname)

    # logic_p3 = (tmhr_p3>=12.3)&(tmhr_p3<=12.42)
    # logic_er2= (lat_er2>=np.min(lat_p3[logic_p3]))&(lat_er2<=np.max(lat_p3[logic_p3]))&(tmhr_er2>=12.0)&(tmhr_er2<=14.0)

    # # figure settings
    # fig = plt.figure(figsize=(8, 6))
    # ax1 = fig.add_subplot(111)
    # ax1.scatter(lat_er2[logic_er2], alt_er2[logic_er2])
    # ax1.scatter(lat_p3[logic_p3], alt_p3[logic_p3])
    # # plt.legend(loc='best', fontsize=12, framealpha=0.4)
    # plt.savefig('test.png')
    # plt.show()
    # exit()

    logic_er2 = (lon_er2>=8.5)&(lon_er2<=9.5)
    logic_p3 = (lon_p3>=8.5)&(lon_p3<=9.5)

    # figure settings
    fig = plt.figure(figsize=(8, 6))
    ax1 = fig.add_subplot(111)
    # ax1.set_ylim([-24, -10])
    # ax1.set_xlim([6, 16])
    # ax1.plot(tmhr, lat)
    # cs1 = ax1.scatter(tmhr_er2[logic_er2], lat_er2[logic_er2], c=alt_er2[logic_er2], vmax=12, vmin=0)
    # cs1 = ax1.scatter(tmhr_p3[logic_p3], lat_p3[logic_p3], c=alt_p3[logic_p3], vmax=12, vmin=0)
    # cs1 = ax1.scatter(tmhr_er2, lat_er2, c=alt_er2, vmax=12, vmin=0)
    # cs1 = ax1.scatter(tmhr_p3, lat_p3, c=alt_p3, vmax=12, vmin=0)
    cs1 = ax1.scatter(tmhr_er2, np.cos(np.deg2rad(sza_er2)), c='red')
    cs1 = ax1.scatter(tmhr_p3, np.cos(np.deg2rad(sza_p3)), c='blue')
    # plt.colorbar(cs1)
    # plt.legend(loc='best', fontsize=12, framealpha=0.4)
    # plt.savefig('p3_test.png')
    # plt.savefig('er2_test.png')
    plt.show()

